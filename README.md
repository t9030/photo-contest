# Photo Contest

# Description
A website for creating and participating in photo contests. The application has two main parts:\
**Organizational** – here, the application owners can organize photo contests and invite contestants or jury.
The jury can vote for a submission with score points and a comment.\
**For photo junkies** – everyone is welcome to register and to participate in contests. 
Junkies with certain ranking can be invited to be jury.

Every content has two phases.
In phase one all the contestants can submit a photo, in phase two the jury does the voting. After the contest finishes
1st, 2nd and 3rd place receive points.



# Setup
* Download JDK 11
* Clone this repository
* Build the project using an IDE (ex InteliJ)
* Run the SQL scripts
  * create-photo_contest-database.sql
  * insert-datab.sql

# Database
![Database](Photo-Contest/src/main/resources/static/assets/database.png "MarineGEO logo")

## Rest API Documentation
[Swagger UI Documentation](http://localhost:8080/swagger-ui.html) (only while application is running)
