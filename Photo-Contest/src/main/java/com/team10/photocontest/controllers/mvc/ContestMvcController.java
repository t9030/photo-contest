package com.team10.photocontest.controllers.mvc;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.AuthenticationFailureException;
import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Comment;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.ContestCreateDTO;
import com.team10.photocontest.models.dto.PhotoSubmissionInDTO;
import com.team10.photocontest.models.dto.InviteDTO;
import com.team10.photocontest.models.dto.VoteDTO;
import com.team10.photocontest.services.contracts.*;
import com.team10.photocontest.utils.mappers.ContestMapper;
import net.bytebuddy.utility.RandomString;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    private static final String REDIRECT = "redirect:/contests/%d";

    private final UserService userService;
    private final ContestService contestService;
    private final CategoryService categoryService;
    private final PhotoSubmissionService photoSubmissionService;
    private final AuthenticationHelper authenticationHelper;
    private final JavaMailSender javaMailSender;
    private final ContestMapper contestMapper;
    private final PhotoService photoService;

    public ContestMvcController(ContestService contestService, AuthenticationHelper authenticationHelper,
                                ContestMapper contestMapper, UserService userService, JavaMailSender javaMailSender,
                                CategoryService categoryService, PhotoSubmissionService photoSubmissionService,
                                PhotoService photoService, CommentService commentService) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.contestMapper = contestMapper;
        this.userService = userService;
        this.javaMailSender = javaMailSender;
        this.categoryService = categoryService;
        this.photoSubmissionService = photoSubmissionService;
        this.photoService = photoService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session) {
        return (User) session.getAttribute("currentUser");
    }

    @GetMapping
    public String showAll(Model model, HttpSession session){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }
        model.addAttribute("allContests", contestService.filter(
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), user));
        model.addAttribute("open", contestService.filter(
                Optional.empty(), Optional.empty(), Optional.of(true),
                Optional.empty(), Optional.empty(), user));
        model.addAttribute("closed", contestService.filter(
                Optional.empty(), Optional.empty(), Optional.of(false),
                Optional.empty(), Optional.empty(), user));
        model.addAttribute("phaseI", contestService.filter(
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of("phaseI"), Optional.empty(), user));
        model.addAttribute("phaseII", contestService.filter(
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of("phaseII"), Optional.empty(), user));
        model.addAttribute("finished", contestService.filter(
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.of("finished"), Optional.empty(), user));
        return "contests";
    }

    @GetMapping("/{id}")
    public String showSingleContestPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }
        try {
            Contest currentContest = contestService.getById(id);
            if (currentContest.getContestants().contains(user)) {
                PhotoSubmission submission = photoSubmissionService.filter(Optional.of(user.getUsername()),
                        Optional.of(currentContest.getId()), Optional.empty(), Optional.empty()).get(0);
                model.addAttribute("submission", submission);
            }

            model.addAttribute("contest", currentContest);
            List<PhotoSubmission> submissions = new LinkedList<>(currentContest.getPhotoSubmissions());
            model.addAttribute("submissions", submissions.stream()
                    .sorted((s1, s2) -> Integer.compare(s2.getScore(), s1.getScore()))
                    .collect(Collectors.toList()));
            model.addAttribute("endDate", contestService.getDateTime(currentContest));
            model.addAttribute("Phase", contestService.getPhase(currentContest));
            model.addAttribute("now", LocalDateTime.now());
            model.addAttribute("vote", new VoteDTO());
            model.addAttribute("phase1", currentContest.getEndPhaseI());
            model.addAttribute("phase2",currentContest.getEndPhaseII());
            model.addAttribute("newSubmission", new PhotoSubmissionInDTO());
            Map<Integer, Comment> voted = new HashMap<>();
            for (PhotoSubmission submission : submissions) {
                if (submission.getComments().stream()
                        .anyMatch(comment -> comment.getAuthor().getId() == user.getId())) {
                    voted.put(submission.getId(),
                            photoSubmissionService.getJuryComment(submission.getId(), user.getId()));
                }
            }
            model.addAttribute("voted", voted);

            if (currentContest.getEndPhaseII().isBefore(LocalDateTime.now())) {
                return "contest-finished";
            } else {
                if (!currentContest.getJury().contains(user)) {
                    return "contest-page-junkies";
                } else {
                    return "contest-page-jury";
                }
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showCreateContestPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("organizerId", user.getId());
            model.addAttribute("action", "create");
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("contest", new ContestCreateDTO());
            model.addAttribute("defaultCovers", photoService.getAll(Optional.of("Cover Photo")));
            return "contest-new";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }
    }

    @PostMapping("/new")
    public String createContest(@Valid @ModelAttribute("contest") ContestCreateDTO createDTO,
                                BindingResult errors, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }

        model.addAttribute("organizerId", user.getId());
        model.addAttribute("action", "create");
        if (errors.hasErrors()) {
            return "contest-new";
        }

        try {
            Contest contest = contestMapper.dtoToObject(createDTO);
            model.addAttribute("new_contest", contest);
            contestService.create(contest, user);
            return format(REDIRECT, contest.getId());
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (IOException | ParseException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        } catch (DuplicateEntityException d){
            errors.rejectValue("title", "contest.error",d.getMessage());
            return "contest-new";
        }
    }

    @GetMapping("/{contestId}/invite_jury")
    public String showInviteJuryPage(@PathVariable int contestId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(contestId);
            List<User> juryUsers = userService.filter(Optional.empty(), Optional.of("User"),
                    Optional.of("Master"), Optional.empty());
            juryUsers.addAll(userService.filter(Optional.empty(), Optional.of("User"),
                    Optional.of("Wise and Benevolent Photo Dictator"), Optional.empty()));
            model.addAttribute("users", juryUsers.stream()
                    .filter(user -> !contest.getJury().contains(user))
                    .collect(Collectors.toList()));
            model.addAttribute("contest", contest);
            model.addAttribute("dto", new InviteDTO());
            return "contest-invite-jury";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/{contestId}/invite_contestants")
    public String showInviteContestantsPage(@PathVariable int contestId, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(contestId);
            List<User> contestants = userService.getAll(Optional.empty()).stream()
                    .filter(user -> !contest.getJury().contains(user)
                            && !contest.getContestants().contains(user)
                            && !contest.getInvited().contains(user))
                    .collect(Collectors.toList());
            model.addAttribute("users", contestants);
            model.addAttribute("contest", contest);
            model.addAttribute("dto", new InviteDTO());
            return "contest-invite-contestants";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }
    }

    @PostMapping("/{contestId}/invite_jury")
    public String inviteJury(@PathVariable int contestId, HttpSession session, Model model,
                             HttpServletRequest request, @ModelAttribute("dto") InviteDTO dto) {
        return createInvitation(contestId, request, dto, session, model, "Jury");
    }

    @PostMapping("/{contestId}/invite_contestants")
    public String inviteContestants(@PathVariable int contestId, HttpSession session, Model model,
                                    HttpServletRequest request, @ModelAttribute("dto") InviteDTO dto) {
        return createInvitation(contestId, request, dto, session, model, "Contestant");
    }

    @GetMapping("/invitation_response")
    public String showInvitationResponse(@Param(value = "token") String token, Model model,
                                         @Param(value = "invite_type") String invite_type,
                                         @Param(value = "id") int id) {
        try {
            userService.getByInvitationToken(token);
            model.addAttribute("token", token);
            model.addAttribute("invite_type", invite_type);
            model.addAttribute("id", id);
            model.addAttribute("contest", contestService.getById(id));
            return "invitation-response";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/invitation_response")
    public String handleInvitationResponse(HttpServletRequest request, Model model) {
        try {
            String token = request.getParameter("token");
            String inviteType = request.getParameter("invite_type");
            int id = Integer.parseInt(request.getParameter("id"));
            boolean state = Boolean.parseBoolean(request.getParameter("state"));
            User user = userService.getByInvitationToken(token);
            if (state){
                Contest contest = contestService.getById(id);
                contestService.addParticipant(contest, user, inviteType);
            }
            else {
                user.setInvitationToken(null);
                userService.update(user, user);
            }
            return "redirect:/auth/login";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    /*For Presentational Purposes*/
    @GetMapping("/{contestId}/finish")
    public String forceFinish(@PathVariable int contestId, HttpSession session, Model model){
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(contestId);
            contest.setEndPhaseII(LocalDateTime.now());
            contestService.update(contest);
            return "redirect:/contests/"+contestId;
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    private String createInvitation(int contestId, HttpServletRequest request, InviteDTO dto,
                                    HttpSession session, Model model, String type) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException a) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(contestId);
            String[] emails = dto.getEmails().split(",");
            for (String email : emails) {
                String token = RandomString.make(45);
                userService.updateInvitationToken(token, email);
                String responseInvitationLink = getSiteUrl(request)
                        + "/contests/invitation_response?token="
                        + token
                        + "&invite_type="
                        + type
                        + "&id="
                        + contestId;
                sendEmail(email, responseInvitationLink, type, contest);
            }
            return "redirect:/contests/"+contestId;
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (MessagingException | UnsupportedEncodingException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    private void sendEmail(String email, String responseInvitationLink, String inviteType, Contest contest)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom("contact@photocontest.com", "Photo Contest Support");
        helper.setTo(email);
        String subject = "You have been invited to participate in a contest";
        String content = "<p>Hello,</p>" +
                "<p>We've invited you to be " + inviteType + " in one of our photo contests \""+ contest.getTitle() +"\".</p>" +
                "<p>Click the link below to reply.</p>" +
                "<p><b><a href=\"" + responseInvitationLink + "\">Respond to Invitation</a><b></p>";
        helper.setSubject(subject);
        helper.setText(content, true);
        javaMailSender.send(message);
    }

    private String getSiteUrl(HttpServletRequest request) {
        String siteUrl = request.getRequestURL().toString();
        return siteUrl.replace(request.getServletPath(), "");
    }

}
