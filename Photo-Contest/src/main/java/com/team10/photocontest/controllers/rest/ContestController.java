package com.team10.photocontest.controllers.rest;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.ContestCreateDTO;
import com.team10.photocontest.services.contracts.ContestService;
import com.team10.photocontest.utils.mappers.ContestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestMapper contestMapper;

    @Autowired
    public ContestController(ContestService contestService,
                             AuthenticationHelper authenticationHelper,
                             ContestMapper contestMapper) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.contestMapper = contestMapper;
    }

    @GetMapping
    public List<Contest> getAll(@RequestParam(required = false) Optional<String> sort,
                                @RequestHeader HttpHeaders headers){
        try{
            authenticationHelper.tryGetUser(headers);
            return new ArrayList<>(contestService.getAll(sort));
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Contest>filter(@RequestHeader HttpHeaders headers,
                               @RequestParam(required = false) Optional<String> title,
                               @RequestParam(required = false) Optional<String> category,
                               @RequestParam(required = false) Optional<Boolean> open,
                               @RequestParam(required = false) Optional<String> phase,
                               @RequestParam(required = false) Optional<String> sort){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.filter(title, category, open, phase, sort, user);
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @PostMapping
    public Contest create(@RequestHeader HttpHeaders headers, @RequestParam String title, @RequestParam int category,
                          @RequestParam boolean isOpen, @RequestParam String endPhaseIDate,
                          @RequestParam int phaseIIDuration, @RequestParam MultipartFile photo){
        try{
            ContestCreateDTO createDTO = new ContestCreateDTO(title, category, isOpen,
                    endPhaseIDate, phaseIIDuration, photo);
            Contest contest = contestMapper.dtoToObject(createDTO);
            User user = authenticationHelper.tryGetUser(headers);
            contestService.create(contest, user);
            return contestService.getByTitle(contest.getTitle());
        }catch (DuplicateEntityException | IOException | ParseException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

}
