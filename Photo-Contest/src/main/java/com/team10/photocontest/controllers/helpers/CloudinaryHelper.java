package com.team10.photocontest.controllers.helpers;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.team10.photocontest.models.Photo;
import com.team10.photocontest.services.contracts.ImageTypeService;
import com.team10.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Component
public class CloudinaryHelper {

    private final PhotoService photoService;
    private final ImageTypeService imageTypeService;

    @Autowired
    public CloudinaryHelper(PhotoService photoService, ImageTypeService imageTypeService) {
        this.photoService = photoService;
        this.imageTypeService = imageTypeService;
    }

    public  Photo uploadCloudinary(MultipartFile multipartFile, String ImageType) throws IOException {
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "photocontest",
                "api_key", "349917412276191",
                "api_secret", "Nbyj1Y_XPRKGBHeacIwpJEJ-dw8",
                "secure", true));
        Map upload_result;
        upload_result = cloudinary.uploader().cloudinary().uploader()
                .upload(multipartFile.getBytes(),ObjectUtils.emptyMap());
        Photo photo = new Photo();
        photo.setType(imageTypeService.getByType(ImageType));
        photo.setContent(upload_result.get("url").toString());
        photoService.create(photo);
        return photo;
    }

}
