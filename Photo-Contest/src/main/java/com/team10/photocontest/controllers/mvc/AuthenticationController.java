package com.team10.photocontest.controllers.mvc;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.AuthenticationFailureException;
import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.LoginDTO;
import com.team10.photocontest.models.dto.UserRegisterDTO;
import com.team10.photocontest.services.contracts.PhotoService;
import com.team10.photocontest.services.contracts.UserService;
import com.team10.photocontest.utils.mappers.UserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final PhotoService photoService;

    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper, PhotoService photoService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.photoService = photoService;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDTO login,
                              BindingResult errors,
                              HttpSession session) {
        if (errors.hasErrors()) {
            return "login";
        }

        try {
            User loggedUser = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", loggedUser);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            errors.rejectValue("username", "auth.error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/signup")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new UserRegisterDTO());
        model.addAttribute("defaultAvatars", photoService.getAll(Optional.of("Default Avatar")));
        return "signup";
    }

    @PostMapping("/signup")
    public String handleRegister(@Valid @ModelAttribute("register") UserRegisterDTO register,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "signup";
        }

        if (!register.getPassword().equals(register.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "password_error",
                    "Password confirmation should match password.");
            return "signup";
        }

        try {
            User user = userMapper.DTOtoObject(register);
            userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException | EntityNotFoundException d) {
            String[] exceptionMessage = d.getMessage().split(" ");
            String fieldName = exceptionMessage[2];
            bindingResult.rejectValue(fieldName, "user_error", d.getMessage());
            return "signup";
        } catch (IOException i) {
            bindingResult.rejectValue("avatar", "avatar_error", i.getMessage());
            return "signup";
        }
    }

}
