package com.team10.photocontest.controllers.mvc;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.AuthenticationFailureException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.PhotoSubmissionInDTO;
import com.team10.photocontest.models.dto.VoteDTO;
import com.team10.photocontest.services.contracts.PhotoSubmissionService;
import com.team10.photocontest.utils.mappers.SubmissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/contests/{contestId}/submissions")
public class PhotoSubmissionMvcController {

    private final PhotoSubmissionService photoSubmissionService;
    private final SubmissionMapper submissionMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhotoSubmissionMvcController(PhotoSubmissionService photoSubmissionService,
                                        SubmissionMapper submissionMapper,
                                        AuthenticationHelper authenticationHelper) {
        this.photoSubmissionService = photoSubmissionService;
        this.submissionMapper = submissionMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session) {
        return (User) session.getAttribute("currentUser");
    }

    @PostMapping("/new")
    public String createSubmission(@ModelAttribute("submission") PhotoSubmissionInDTO submissionDTO,
                                   BindingResult errors, @PathVariable int contestId, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            submissionDTO.setAuthorId(user.getId());
            PhotoSubmission photoSubmission = submissionMapper.DTOtoObject(submissionDTO, contestId);
            photoSubmissionService.create(photoSubmission, contestId);
            return "redirect:/contests/" + contestId;
        } catch (RuntimeException | IOException e) {
            errors.rejectValue("photo", "submission.error", e.getMessage());
            return "redirect:/contests/" + contestId;
        }
    }

    @PostMapping("/{submissionId}/vote")
    public String vote(@PathVariable int contestId, @PathVariable int submissionId, HttpSession session,
                       @ModelAttribute("vote") VoteDTO voteDTO) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        voteDTO.setSubmissionId(submissionId);
        voteDTO.setAuthorId(user.getId());
        submissionMapper.voteObject(voteDTO);
        return "redirect:/contests/" + contestId;
    }

    @GetMapping("/{submissionId}/delete")
    public String delete(@PathVariable int contestId, @PathVariable int submissionId,
                         HttpSession session, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            photoSubmissionService.delete(submissionId, user);
            return "redirect:/contests/"+contestId;
        }catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }
    }
}
