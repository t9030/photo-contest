package com.team10.photocontest.controllers.mvc;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.AuthenticationFailureException;
import com.team10.photocontest.models.User;
import com.team10.photocontest.services.contracts.ContestService;
import com.team10.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/search")
public class SearchController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ContestService contestService;

    @Autowired
    public SearchController(AuthenticationHelper authenticationHelper, UserService userService,
                            ContestService contestService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.contestService = contestService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session){
        return (User) session.getAttribute("currentUser");
    }

    @GetMapping
    public String showAll(HttpSession session, HttpServletRequest request, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        String searchTerm = request.getParameter("searchTerm");
        model.addAttribute("users", userService.getAll(Optional.of(searchTerm)));
        model.addAttribute("contests", contestService.getAll(Optional.of(searchTerm))
                .stream()
                .filter(contest -> contest.isOpen() ||
                        contest.getContestants().contains(user) ||
                        contest.getInvited().contains(user) ||
                        contest.getJury().contains(user))
                .collect(Collectors.toList()));
        return "search-result";
    }
}
