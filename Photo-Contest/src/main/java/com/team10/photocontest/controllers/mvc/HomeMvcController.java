package com.team10.photocontest.controllers.mvc;

import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.PhotoSubmissionOutDTO;
import com.team10.photocontest.services.contracts.PhotoSubmissionService;
import com.team10.photocontest.utils.mappers.SubmissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final PhotoSubmissionService photoSubmissionService;
    private final SubmissionMapper submissionMapper;

    @Autowired
    public HomeMvcController(PhotoSubmissionService photoSubmissionService,
                             SubmissionMapper submissionMapper) {
        this.photoSubmissionService = photoSubmissionService;
        this.submissionMapper = submissionMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session){
        return (User) session.getAttribute("currentUser");
    }

    @GetMapping
    public String showHomePage(Model model) {
        List<PhotoSubmissionOutDTO> submissions =
                photoSubmissionService.getAll(Optional.empty())
                .stream().map(submissionMapper::ObjectToDTO)
                .collect(Collectors.toList());
        model.addAttribute("firstRow", submissions.stream()
                .limit(7)
                .collect(Collectors.toList()));
        model.addAttribute("secondRow", new ArrayList<>(submissions).stream()
                .sorted(Comparator.comparing(PhotoSubmissionOutDTO::getTitle))
                .limit(7)
                .collect(Collectors.toList()));
        model.addAttribute("thirdRow", new ArrayList<>(submissions).stream()
                .sorted(Comparator.comparing(PhotoSubmissionOutDTO::getScore).reversed())
                .limit(7)
                .collect(Collectors.toList()));
        return "index";
    }
}
