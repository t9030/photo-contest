package com.team10.photocontest.controllers.rest;

import com.team10.photocontest.controllers.helpers.CloudinaryHelper;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.Photo;
import com.team10.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/photos")
public class PhotoController {

    private final PhotoService photoService;
    private final CloudinaryHelper cloudinaryHelper;

    @Autowired
    public PhotoController(PhotoService photoService, CloudinaryHelper cloudinaryHelper) {
        this.photoService = photoService;
        this.cloudinaryHelper = cloudinaryHelper;
    }

    @GetMapping("/{id}")
    public Photo getById(@PathVariable int id) {
        try {
            return photoService.getById(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<Photo> getAll(@RequestParam(required = false) Optional<String> search) {
        try {
            return photoService.getAll(search);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Photo create(@RequestParam MultipartFile multipartImage,
                           @RequestParam String imageType) throws Exception {
        Photo photo = cloudinaryHelper.uploadCloudinary(multipartImage, imageType);
        photoService.create(photo);
        return photo;
    }
}
