package com.team10.photocontest.controllers.rest;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.PhotoSubmissionInDTO;
import com.team10.photocontest.models.dto.VoteDTO;
import com.team10.photocontest.services.contracts.PhotoSubmissionService;
import com.team10.photocontest.utils.mappers.SubmissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests/{contestId}/submissions")
public class PhotoSubmissionController {

    private final PhotoSubmissionService photoSubmissionService;
    private final AuthenticationHelper authenticationHelper;
    private final SubmissionMapper submissionMapper;

    @Autowired
    public PhotoSubmissionController(PhotoSubmissionService photoSubmissionService,
                                     AuthenticationHelper authenticationHelper,
                                     SubmissionMapper submissionMapper) {
        this.photoSubmissionService = photoSubmissionService;
        this.authenticationHelper = authenticationHelper;
        this.submissionMapper = submissionMapper;
    }

    @GetMapping
    public List<PhotoSubmission> getAll(@RequestParam(required = false) Optional<String> search,
                                        @PathVariable int contestId){
        return photoSubmissionService.getAll(search);
    }

    @GetMapping("/{id}")
    public PhotoSubmission getById(@PathVariable int id, @PathVariable int contestId){
        return photoSubmissionService.getById(id);
    }

    @GetMapping("/filter")
    public List<PhotoSubmission> filter(@RequestHeader HttpHeaders headers,
                                        @RequestParam(required = false) Optional<String> authorUsername,
                                        @RequestParam(required = false) Optional<Integer> contestID,
                                        @RequestParam(required = false) Optional<String> contestCategory,
                                        @RequestParam(required = false) Optional<String> sort,
                                        @PathVariable int contestId){
        try {
            authenticationHelper.tryGetUser(headers);
            return photoSubmissionService.filter(authorUsername, contestID, contestCategory, sort);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/new")
    public PhotoSubmission create(@RequestHeader HttpHeaders headers, @PathVariable int contestId,
                                  @RequestParam String title,
                                  @RequestParam String story,
                                  @RequestParam MultipartFile photo){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            PhotoSubmissionInDTO submissionDTO = new PhotoSubmissionInDTO(title, story, photo, user.getId());
            PhotoSubmission photoSubmission = submissionMapper.DTOtoObject(submissionDTO, contestId);
            photoSubmissionService.create(photoSubmission, contestId);
            return photoSubmission;
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }catch (IOException i){
            throw new ResponseStatusException(HttpStatus.CONFLICT, i.getMessage());
        }
    }

@PostMapping("/{id}/vote")
    public PhotoSubmission vote(@RequestHeader HttpHeaders headers, @PathVariable int contestId,@PathVariable int id,
                                @RequestBody VoteDTO voteDTO){
    try {PhotoSubmission photoSubmission = photoSubmissionService.getById(id);
        User user = authenticationHelper.tryGetUser(headers);
//        VoteDTO voteDTO = new VoteDTO(Integer.parseInt(score),comment,Boolean.parseBoolean(wrongCategory),
//                user.getId(), id);
         submissionMapper.voteObject(voteDTO);
        return photoSubmission;
    }catch (UnauthorizedOperationException e) {
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
    }catch (EntityNotFoundException e){
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
    }
}
}
