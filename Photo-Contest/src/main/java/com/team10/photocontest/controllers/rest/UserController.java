package com.team10.photocontest.controllers.rest;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.controllers.helpers.CloudinaryHelper;
import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Photo;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.UserOutDTO;
import com.team10.photocontest.models.dto.UserRegisterDTO;
import com.team10.photocontest.models.dto.UserUpdateDTO;
import com.team10.photocontest.services.contracts.UserService;
import com.team10.photocontest.utils.mappers.UserMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final CloudinaryHelper cloudinaryHelper;

    public UserController(UserService userService, AuthenticationHelper authenticationHelper,
                          UserMapper userMapper, CloudinaryHelper cloudinaryHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.cloudinaryHelper = cloudinaryHelper;
    }

    @GetMapping
    public List<UserOutDTO> getAll(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) Optional<String> search){
        try {
            authenticationHelper.tryGetUser(headers);
            return userService.getAll(search).stream()
                    .map(userMapper::ObjectToDTO)
                    .collect(Collectors.toList());
        }catch (UnauthorizedOperationException u){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/{id}")
    public UserOutDTO getById(@PathVariable int id, @RequestHeader HttpHeaders headers){
        try {
            authenticationHelper.tryGetUser(headers);
            return userMapper.ObjectToDTO(userService.getById(id));
        }catch (UnauthorizedOperationException u){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<UserOutDTO> filter(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) Optional<String> username,
                                   @RequestParam(required = false) Optional<String> roleName,
                                   @RequestParam(required = false) Optional<String> rankName,
                                   @RequestParam(required = false) Optional<String> sort){
        try {
            authenticationHelper.tryGetUser(headers);
            return userService.filter(username, roleName, rankName, sort)
                    .stream()
                    .map(userMapper::ObjectToDTO)
                    .collect(Collectors.toList());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnsupportedOperationException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PostMapping
    public User create(@Valid @RequestBody UserRegisterDTO userRegisterDTO){
        try {
            User user = userMapper.DTOtoObject(userRegisterDTO);
            userService.create(user);
            return user;
        } catch (DuplicateEntityException | IOException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
    
    @PutMapping("/{id}/upload/avatar")
    public int uploadImage(@RequestParam MultipartFile multipartImage, @PathVariable int id,
                           @RequestHeader HttpHeaders headers) {
        try {
            User updater = authenticationHelper.tryGetUser(headers);

            Photo avatar = cloudinaryHelper.uploadCloudinary(multipartImage, "Avatar");
            User user = userService.getById(id);
            user.setAvatar(avatar);
            userService.update(user, updater);
            return avatar.getId();
        }
        catch (IOException i){
            throw new ResponseStatusException(HttpStatus.CONFLICT, i.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping()
    public User update(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody UserUpdateDTO userToUpdate) {
        try {
            User updater = authenticationHelper.tryGetUser(headers);
            User user = userMapper.UpdateDTOtoUser(userToUpdate);
            userService.update(user, updater);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException | IOException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id, user);
        }catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
