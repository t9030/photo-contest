package com.team10.photocontest.controllers.mvc;

import com.team10.photocontest.controllers.helpers.AuthenticationHelper;
import com.team10.photocontest.exceptions.AuthenticationFailureException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Category;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.PhotoSubmissionOutDTO;
import com.team10.photocontest.models.dto.UserOutDTO;
import com.team10.photocontest.models.dto.UserUpdateDTO;
import com.team10.photocontest.services.contracts.*;
import com.team10.photocontest.utils.mappers.SubmissionMapper;
import com.team10.photocontest.utils.mappers.UserMapper;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    public static String PASSWORD_WARNING = "Password should be at least 8 symbols";

    private final UserService userService;
    private final JavaMailSender javaMailSender;
    private final AuthenticationHelper authenticationHelper;
    private final PhotoSubmissionService photoSubmissionService;
    private final CategoryService categoryService;
    private final UserMapper userMapper;
    private final SubmissionMapper submissionMapper;
    private final PhotoService photoService;
    private final ContestService contestService;

    @Autowired
    public UserMvcController(UserService userService, JavaMailSender javaMailSender,
                             AuthenticationHelper authenticationHelper,
                             PhotoSubmissionService photoSubmissionService,
                             CategoryService categoryService, UserMapper userMapper,
                             SubmissionMapper submissionMapper, PhotoService photoService,
                             ContestService contestService) {
        this.userService = userService;
        this.javaMailSender = javaMailSender;
        this.authenticationHelper = authenticationHelper;
        this.photoSubmissionService = photoSubmissionService;
        this.categoryService = categoryService;
        this.userMapper = userMapper;
        this.submissionMapper = submissionMapper;
        this.photoService = photoService;
        this.contestService = contestService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("authenticatedUser")
    public User populateAuthenticatedUser(HttpSession session){
        return (User) session.getAttribute("currentUser");
    }

    @ModelAttribute("categories")
    public List<Category> populateAuthenticatedUser(){
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    public String showProfile(@PathVariable int id, Model model, HttpSession session){
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            UserOutDTO user = userMapper.ObjectToDTO(userService.getById(id));
            List<PhotoSubmissionOutDTO> submissions = photoSubmissionService
                            .filter(Optional.ofNullable(user.getUsername()), Optional.empty(),
                                    Optional.empty(), Optional.empty())
                            .stream().map(submissionMapper::ObjectToDTO)
                            .collect(Collectors.toList());
            model.addAttribute("user", user);
            model.addAttribute("top", userService.topPlace(id));
            model.addAttribute("topCategory", userService.mostFrequentCategory(id));
            model.addAttribute("submissions", submissions);
            return "profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdatePage(@PathVariable int id, Model model, HttpSession session){
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            UserUpdateDTO userUpdateDTO = userMapper.objectToUpdateDto(user);
            model.addAttribute("defaultAvatars", photoService.getAll(Optional.of("Default Avatar")));
            model.addAttribute("userToUpdate", userUpdateDTO);
            return "user-update";
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/contests")
    public String showMyContests(@PathVariable int id,  Model model, HttpSession session){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            model.addAttribute("allContests", contestService.filter(
                    Optional.empty(), Optional.empty(),
                    Optional.empty(), Optional.empty(), Optional.empty(), user));
            model.addAttribute("phaseIContests", contestService.filter(
                    Optional.empty(), Optional.empty(),
                    Optional.empty(), Optional.of("phaseI"), Optional.empty(), user));
            model.addAttribute("phaseIIContests", contestService.filter(
                    Optional.empty(), Optional.empty(),
                    Optional.empty(), Optional.of("phaseII"), Optional.empty(), user));
            model.addAttribute("juryContests", contestService.getAll(Optional.empty())
                    .stream()
                    .filter(contest -> contest.getJury().contains(user))
                    .collect(Collectors.toList()));
            model.addAttribute("finishedContests", contestService.filter(
                    Optional.empty(), Optional.empty(),
                    Optional.empty(), Optional.of("finished"), Optional.empty(), user));
            return "my-contests";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }catch (Exception u){
            model.addAttribute("error", u.getMessage());
            return "error-page";
        }
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable int id,
                             @Valid @ModelAttribute("userToUpdate") UserUpdateDTO updateUserDTO,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "user-update";
        }

        try {
            User userToUpdate = userMapper.UpdateDTOtoUser(updateUserDTO);
            if (!updateUserDTO.getCurrentPassword().isEmpty() || !updateUserDTO.getNewPassword().isEmpty()){
                if (updateUserDTO.getCurrentPassword().equals(user.getPassword())){
                    userToUpdate.setPassword(updateUserDTO.getNewPassword());
                }
                else {
                    bindingResult.rejectValue("currentPassword", "user_error",
                            "Wrong password");
                    return "user-update";
                }
            }
            userService.update(userToUpdate, user);
            return "redirect:/users/"+id;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "unauthorized";
        } catch (IOException e) {
            bindingResult.rejectValue("multipartAvatar", "user_error", e.getMessage());
            return "user-update";
        }
    }

    @GetMapping("/{id}/promote")
    public String promote(@PathVariable int id, HttpSession session, Model model){
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.promote(id, user);
            return "redirect:/users/"+id;
        } catch (UnauthorizedOperationException u){
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }
    }

    @GetMapping("/forgotten_password")
    public String showResetPassword(){
        return "forgotten-password";
    }

    @PostMapping("/forgotten_password")
    public String handleReset(HttpServletRequest request, Model model){
        try {
            String email = request.getParameter("email");
            String token = RandomString.make(45);
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = getSiteUrl(request) + "/users/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            model.addAttribute("message",
                    "We have sent a reset password link to your email. Please check.");
            return "forgotten-password";
        } catch (MessagingException | UnsupportedEncodingException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
    }

    @GetMapping("/reset_password")
    public String showResetPassword(@Param(value = "token") String token, Model model){
        try {
            userService.getByResetPasswordToken(token);
            model.addAttribute("token", token);
            return "reset-password";
        }catch (EntityNotFoundException e){
            return "not-found";
        }
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");
        if(password.length() < 8){
            model.addAttribute("errors", PASSWORD_WARNING);
            return "reset-password";
        }
        User user = userService.getByResetPasswordToken(token);
        userService.updatePassword(user, password);
        return "redirect:/auth/login";
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, user);
        } catch (UnauthorizedOperationException u) {
            model.addAttribute("error", u.getMessage());
            return "unauthorized";
        }

        return "redirect:/auth/logout";
    }

    private void sendEmail(String email, String resetPasswordLink)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom("contact@photocontest.com", "Photo Contest Support");
        helper.setTo(email);
        String subject = "Here is the link to reset your password";
        String content = "<p>Hello,</p>" +
                "<p>You have requested to reset your password.</p>" +
                "<p>Click the link below to change your password:</p>" +
                "<p><b><a href=\"" + resetPasswordLink + "\">Change my password</a><b></p>" +
                "<p>Ignore this email if you do remember your password, or you have not made this request</p>";
        helper.setSubject(subject);
        helper.setText(content, true);
        javaMailSender.send(message);
    }

    private String getSiteUrl(HttpServletRequest request){
        String siteUrl = request.getRequestURL().toString();
        return siteUrl.replace(request.getServletPath(), "");
    }

}
