package com.team10.photocontest.services;

import com.team10.photocontest.models.Comment;
import com.team10.photocontest.repositories.contracts.CommentRepository;
import com.team10.photocontest.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getAll() {
        return commentRepository.getAll();
    }

    @Override
    public Comment getById(int id) {
        return commentRepository.getById(id);
    }

    @Override
    public List<Comment> filter(Optional<String> authorUsername,
                                Optional<Integer> submissionId,
                                Optional<String> sort) {
        return commentRepository.filter(authorUsername, submissionId, sort);
    }

    @Override
    public void create(Comment comment) {
        commentRepository.create(comment);
    }

    @Override
    public void update(Comment comment) {
        commentRepository.update(comment);
    }

    @Override
    public void delete(int id) {
        commentRepository.delete(id);
    }
}
