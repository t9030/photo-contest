package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentService {

    List<Comment> getAll();

    Comment getById(int id);

    List<Comment> filter(Optional<String> authorUsername, Optional<Integer> submissionId, Optional<String> sort);

    void create(Comment comment);

    void update(Comment comment);

    void delete(int id);
}
