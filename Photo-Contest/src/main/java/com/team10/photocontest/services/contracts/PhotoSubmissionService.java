package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Comment;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface PhotoSubmissionService {

    PhotoSubmission getById(int id);

    List<PhotoSubmission> getAll(Optional<String> search);

    List<PhotoSubmission> filter(Optional<String> authorUsername, Optional<Integer> contestId,
                                 Optional<String> contestCategory, Optional<String> sort);

    void create(PhotoSubmission photoSubmission, int contestId);

    void comment(int submissionId, Comment comment);

    Comment getJuryComment(int submissionId, int userId);

    void rate(int submissionId, int score);

    //void update(PhotoSubmission photoSubmission);

    void delete(int id, User user);

}
