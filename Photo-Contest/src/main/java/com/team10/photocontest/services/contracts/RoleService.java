package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Role;

import java.util.List;

public interface RoleService {

    Role getById(int id);

    Role getByName(String role);

    List<Role> getAll();

    void create(Role role);

    void update(Role role);
}
