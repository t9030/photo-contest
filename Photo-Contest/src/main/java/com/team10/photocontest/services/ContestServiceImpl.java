package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.ContestRepository;
import com.team10.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.team10.photocontest.services.contracts.ContestService;
import com.team10.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestRepository contestRepository;
    private final UserService userService;
    private final PhotoSubmissionRepository photoSubmissionRepository;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, UserService userService,
                              PhotoSubmissionRepository photoSubmissionRepository) {
        this.contestRepository = contestRepository;
        this.userService = userService;
        this.photoSubmissionRepository = photoSubmissionRepository;
    }

    @Override
    public List<Contest> getAll(Optional<String> search) {
        return contestRepository.search(search);
    }

    @Override
    public Contest getById(int id) {
        scoring(id);
        return contestRepository.getById(id);
    }

    @Override
    public void create(Contest contest, User user) {
        boolean duplicateContest = true;
        if (!user.getRole().getRole().equals("Organizer")) {
            throw new UnauthorizedOperationException("Only Organizers can create contests");
        }
        try {
            contestRepository.getByField("title", contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateContest = false;
        }
        if (duplicateContest) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        Set<User> jury = new HashSet<>(userService.filter(Optional.empty(),
                Optional.of("Organizer"),
                Optional.empty(),
                Optional.empty()));
        contest.setJury(jury);
        contestRepository.create(contest);

    }

    @Override
    public Contest getByTitle(String title) {
        return contestRepository.getByField("title", title);
    }

    @Override
    public void update(Contest contest) {
        contestRepository.update(contest);
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<String> category,
                                Optional<Boolean> open, Optional<String> phase,
                                Optional<String> sort, User user) {
        return contestRepository.filter(title, category, open, phase, sort)
                .stream()
                .filter(contest -> contest.isOpen() ||
                        contest.getContestants().contains(user) ||
                        contest.getInvited().contains(user) ||
                        contest.getJury().contains(user))
                .collect(Collectors.toList());
    }

    @Override
    public String getPhase(Contest contest){
        String phase;
        if (contest.getEndPhaseI().isAfter(LocalDateTime.now())) {
            phase = "Contest is in Phase I - Photo Submissions";
        } else if (contest.getEndPhaseII().isAfter(LocalDateTime.now())) {
            phase = "Contest is in Phase II - Jury Votes";
        } else {
            phase = "This contest is already finished";
        }
        return phase;
    }

    @Override
    public LocalDateTime getDateTime(Contest contest) {
        LocalDateTime timestamp;
        if (contest.getEndPhaseI().isAfter(LocalDateTime.now())) {
            timestamp = contest.getEndPhaseI();
        } else {
            timestamp = contest.getEndPhaseII();
        }
        return timestamp;
    }

    @Override
    public void addParticipant(Contest contest, User user, String inviteType) {
        if (inviteType.equals("Jury")) {
            Set<User> jury = contest.getJury();
            jury.add(user);
            contest.setJury(jury);
        } else {
            Set<User> invited = contest.getInvited();
            invited.add(user);
            contest.setInvited(invited);
        }

        contestRepository.update(contest);
        user.setInvitationToken(null);
        userService.update(user, user);
    }

    public void scoring(int id) {
        Contest contest = contestRepository.getById(id);
        if (contest.getEndPhaseII().isBefore(LocalDateTime.now())
                && contest.getPhotoSubmissions().size() > 0
                && !contest.isScored()) {
            List<PhotoSubmission> sortedSubmissions = contest.getPhotoSubmissions().stream()
                    .sorted((s1, s2) -> Integer.compare(s2.getScore(), s1.getScore()))
                    .collect(Collectors.toList());

            int place = 1;
            int counter = 1;
            unvoted(contest);
            for (int i = 0; i < sortedSubmissions.size() && place < 4; i++) {

                if (i == sortedSubmissions.size() - 1 ||
                        sortedSubmissions.get(i).getScore() != sortedSubmissions.get(i + 1).getScore()) {
                    if (counter != 1) {
                        int userIndex = i;
                        while (counter > 0) {
                            User user = sortedSubmissions.get(userIndex).getAuthor();
                            int newPoints = user.getPoints() + 40 - (place - 1) * 15;
                            user.setPoints(newPoints);
                            userService.update(user, user);
                            userIndex--;
                            counter--;
                        }
                    } else {
                        User user = sortedSubmissions.get(i).getAuthor();
                        if (sortedSubmissions.size() == 1 || (place == 1 && sortedSubmissions.get(i).getScore() >=
                                2 * sortedSubmissions.get(i + 1).getScore())) {
                            user.setPoints(user.getPoints() + 75);
                        } else {
                            int newPoints = user.getPoints() + 50 - (place - 1) * 15;
                            user.setPoints(newPoints);
                        }
                        userService.update(user, user);
                        counter--;
                    }

                    place++;
                }

                counter++;
            }

            contest.setScored(true);
            update(contest);
        }
    }

    public void unvoted(Contest contest){
        for(PhotoSubmission submission: contest.getPhotoSubmissions()) {
            if (submission.getComments().size() == 0) {
                submission.setScore(3);
            }
            photoSubmissionRepository.update(submission);
        }
    }

}
