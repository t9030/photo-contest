package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User getById(int id);

    User getByUsername(String username);

    User getByResetPasswordToken(String token);

    User getByEmail(String email);

    String topPlace(int id);

    String[] mostFrequentCategory(int userId);

    List<User> getAll(Optional<String> search);

    List<User> filter(Optional<String> username, Optional<String> roleName,
                      Optional<String> rankName, Optional<String> sort);

    void create(User user);

    void update(User userToUpdate, User user);

    void promote(int userToPromoteId, User user);

    void updateResetPasswordToken(String token, String email);

    User getByInvitationToken(String token);

    void updateInvitationToken(String token, String email);

    void updatePassword(User user, String password);

    void delete(int id, User user);

}
