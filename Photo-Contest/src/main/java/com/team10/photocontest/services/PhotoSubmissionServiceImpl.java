package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Comment;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.CommentRepository;
import com.team10.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.team10.photocontest.services.contracts.ContestService;
import com.team10.photocontest.services.contracts.PhotoSubmissionService;
import com.team10.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PhotoSubmissionServiceImpl implements PhotoSubmissionService {

    private static final String UNAUTHORIZED_DELETE = "Only a member of the contest's jury can delete submissions!";

    private final PhotoSubmissionRepository photoSubmissionRepository;
    private final CommentRepository commentRepository;
    private final ContestService contestService;
    private final UserService userService;

    @Autowired
    public PhotoSubmissionServiceImpl(PhotoSubmissionRepository photoSubmissionRepository,
                                      CommentRepository commentRepository, ContestService contestService,
                                      UserService userService) {
        this.photoSubmissionRepository = photoSubmissionRepository;
        this.commentRepository = commentRepository;
        this.contestService = contestService;
        this.userService = userService;
    }

    @Override
    public PhotoSubmission getById(int id) {
        return photoSubmissionRepository.getById(id);
    }

    @Override
    public List<PhotoSubmission> getAll(Optional<String> search) {
        return photoSubmissionRepository.search(search);
    }

    @Override
    public List<PhotoSubmission> filter(Optional<String> authorUsername, Optional<Integer> contestId,
                                        Optional<String> contestCategory, Optional<String> sort) {
        return photoSubmissionRepository.filter(authorUsername, contestId, contestCategory, sort);
    }

    @Override
    public void create(PhotoSubmission photoSubmission, int contestId) {
        photoSubmissionRepository.create(photoSubmission);
        Contest contest = contestService.getById(contestId);
        User user = userService.getByUsername(photoSubmission.getAuthor().getUsername());

        Set<PhotoSubmission> submissionSet = contest.getPhotoSubmissions();
        submissionSet.add(photoSubmission);
        contest.setPhotoSubmissions(submissionSet);

        Set<User> contestants = contest.getContestants();
        contestants.add(user);
        contest.setContestants(contestants);

        if (contest.getInvited().contains(user)){
            user.setPoints(user.getPoints() + 3);
            Set<User> invited = contest.getInvited();
            invited.remove(user);
            contest.setInvited(invited);
        }else {
            user.setPoints(user.getPoints() + 1);
        }

        userService.update(user, user);
        contestService.update(contest);
    }

    @Override
    public void comment(int submissionId, Comment comment) {
        commentRepository.create(comment);
        PhotoSubmission submission = getById(submissionId);
        Set<Comment> newComments = submission.getComments();
        newComments.add(comment);
        submission.setComments(newComments);
        photoSubmissionRepository.update(submission);
    }

    @Override
    public Comment getJuryComment(int submissionId, int userId){
        PhotoSubmission submission = getById(submissionId);
        return submission.getComments().stream()
                .filter(comment -> comment.getAuthor().getId() == userId)
                .findFirst().get();
    }

    @Override
    public void rate(int submissionId, int score) {
        PhotoSubmission submission = getById(submissionId);
        submission.setScore(submission.getScore() + score);
        photoSubmissionRepository.update(submission);
    }

    @Override
    public void delete(int id, User user) {
        PhotoSubmission submissionToDel = getById(id);
        Contest contest = contestService.getById(submissionToDel.getContestId());
        if (!contest.getJury().contains(user)){
            throw new UnauthorizedOperationException(UNAUTHORIZED_DELETE);
        }
        Set<User> newContestants = contest.getContestants();
        newContestants.remove(submissionToDel.getAuthor());
        contest.setContestants(newContestants);
        contestService.update(contest);
        photoSubmissionRepository.delete(id);
    }
}
