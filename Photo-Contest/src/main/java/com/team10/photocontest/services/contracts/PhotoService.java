package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Photo;

import java.util.List;
import java.util.Optional;

public interface PhotoService {

    List<Photo> getAll(Optional<String> search);

    Photo getById(int id);

    Photo getByContent(String content);

    void create(Photo photo);

}
