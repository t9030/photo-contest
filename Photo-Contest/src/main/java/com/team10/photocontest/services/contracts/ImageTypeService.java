package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.ImageType;

import java.util.List;

public interface ImageTypeService {

    ImageType getById(int id);

    ImageType getByType(String type);

    List<ImageType> getAll();

    void create(ImageType type);

    void update(ImageType type);
}
