package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ContestService {

    List<Contest> getAll(Optional<String> search);

    Contest getById(int id);

    void create(Contest contest, User user);

    Contest getByTitle(String title);

    void update(Contest contest);

    List<Contest> filter(Optional<String> title, Optional<String> category,
                         Optional<Boolean> open, Optional<String> phase,
                         Optional<String> sort, User user);

    String getPhase(Contest contest);

    LocalDateTime getDateTime(Contest contest);

    void addParticipant(Contest contest, User user, String inviteType);
}
