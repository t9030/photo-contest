package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.ImageType;
import com.team10.photocontest.repositories.contracts.ImageTypeRepository;
import com.team10.photocontest.services.contracts.ImageTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageTypeServiceImpl implements ImageTypeService {

    private final ImageTypeRepository imageTypeRepository;

    @Autowired
    public ImageTypeServiceImpl(ImageTypeRepository imageTypeRepository) {
        this.imageTypeRepository = imageTypeRepository;
    }

    @Override
    public ImageType getById(int id) {
        return imageTypeRepository.getById(id);
    }

    @Override
    public ImageType getByType(String type) {
        return imageTypeRepository.getByField("type", type);
    }

    @Override
    public List<ImageType> getAll() {
        return imageTypeRepository.getAll();
    }

    @Override
    public void create(ImageType type) {
        boolean duplicateType = true;

        try {
            imageTypeRepository.getByField("type", type.getType());
        } catch (EntityNotFoundException e) {
            duplicateType = false;
        }
        if (duplicateType) {
            throw new DuplicateEntityException("ImageType", "type", type.getType());
        }

        imageTypeRepository.create(type);
    }

    @Override
    public void update(ImageType type) {
        boolean duplicateType = true;

        try {
            imageTypeRepository.getByField("type", type.getType());
        } catch (EntityNotFoundException e) {
            duplicateType = false;
        }
        if (duplicateType) {
            throw new DuplicateEntityException("ImageType", "type", type.getType());
        }

        imageTypeRepository.update(type);
    }
}
