package com.team10.photocontest.services;

import com.team10.photocontest.models.Photo;
import com.team10.photocontest.repositories.contracts.PhotoRepository;
import com.team10.photocontest.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public List<Photo> getAll(Optional<String> search) {
        return photoRepository.search(search);
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

    @Override
    public Photo getByContent(String content) {
        return photoRepository.getByField("content", content);
    }

    @Override
    public void create(Photo photo) {
        photoRepository.create(photo);
    }
}
