package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Category;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.*;
import com.team10.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final String UNAUTHORIZED_UPDATE = "Only the owner of the account can make changes";
    private static final String UNAUTHORIZED_DELETE = "Only the owner of the account can delete it";
    private static final String UNAUTHORIZED_PROMOTE = "Only organizers can promote other users to that role";

    private static final int MIN_ENTHUSIAST = 51;
    private static final int MAX_ENTHUSIAST = 150;
    private static final int MIN_MASTER = 151;
    private static final int MAX_MASTER = 1000;
    private static final int MIN_DICTATOR = 1001;

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final RankRepository rankRepository;
    private final CategoryRepository categoryRepository;
    private final ContestRepository contestRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
                           RankRepository rankRepository, CategoryRepository categoryRepository,
                           ContestRepository contestRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.rankRepository = rankRepository;
        this.categoryRepository = categoryRepository;
        this.contestRepository = contestRepository;
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByField("email", email);
    }

    @Override
    public User getByResetPasswordToken(String token) {
        return userRepository.getByField("resetPasswordToken", token);
    }

    @Override
    public User getByInvitationToken(String token){
        return userRepository.getByField("invitation_token", token);
    }

    @Override
    public String topPlace(int id) {
        return userRepository.topPlace(id);
    }

    @Override
    public String[] mostFrequentCategory(int userId){
        User user = getById(userId);
        List<Contest> contests = contestRepository.getAll()
                .stream().filter(contest -> contest.getContestants().contains(user))
                .collect(Collectors.toList());

        String[] result = new String[2];
        if (contests.isEmpty()){
            result[0] = "No Categories";
            result[1] = "0";
            return result;
        }

        Map<Category, Integer> occurrences = new HashMap<>();
        List<Category> categories = categoryRepository.getAll();
        Category maxCategory = categories.get(0);
        int lastMax = 0;

        for (Category category : categories) {
            int counter = 0 ;
            for (Contest contest : contests) {
                if (contest.getCategory().getCategory().equals(category.getCategory())) {
                    counter++;
                }
            }

            if (counter > lastMax){
                lastMax = counter;
                maxCategory = category;
            }
            occurrences.put(category, counter);
        }

        int all = occurrences.values().stream().mapToInt(Integer::intValue).sum();
        int max = occurrences.values().stream().mapToInt(Integer::intValue).max().getAsInt();
        result[0] = maxCategory.getCategory();
        result[1] = String.valueOf(max * 100/all);
        return result;
    }

    @Override
    public List<User> getAll(Optional<String> search) {
        return userRepository.search(search);
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> roleName,
                             Optional<String> rankName, Optional<String> sort) {
        return userRepository.filter(username, roleName, rankName, sort);
    }

    @Override
    public void create(User user) {
        boolean duplicateUser = true;

        try {
            userRepository.getByField("username" ,user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUser = false;
        }
        if (duplicateUser) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        duplicateUser = true;

        try {
            userRepository.getByField("email" ,user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateUser = false;
        }
        if (duplicateUser) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.create(user);
    }

    @Override
    public void update(User userToUpdate, User user) {
        int points = userToUpdate.getPoints();

        if (points >= MIN_ENTHUSIAST && points <= MAX_ENTHUSIAST){
            userToUpdate.setRank(rankRepository.getByField("rank", "Enthusiast"));
        }
        else if (points >= MIN_MASTER && points <= MAX_MASTER){
            userToUpdate.setRank(rankRepository.getByField("rank", "Master"));
        }
        else if (points >= MIN_DICTATOR){
            userToUpdate.setRank(rankRepository.getByField("rank", "Wise and Benevolent Photo Dictator"));
        }

        if (userToUpdate.getId() != user.getId()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_UPDATE);
        }

        userRepository.update(userToUpdate);
    }

    @Override
    public void promote(int userToPromoteId, User user) {
        User userToPromote = getById(userToPromoteId);

        if (!user.getRole().getRole().equals("Organizer")){
            throw new UnauthorizedOperationException(UNAUTHORIZED_PROMOTE);
        }

        userToPromote.setRole(roleRepository.getByField("role", "Organizer"));
        userRepository.update(userToPromote);
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = getByEmail(email);
        user.setResetPasswordToken(token);
        userRepository.update(user);
    }

    @Override
    public void updatePassword(User user, String password) {
        user.setPassword(password);
        user.setResetPasswordToken(null);
        userRepository.update(user);
    }

    @Override
    public void updateInvitationToken(String token, String email) {
        User user = userRepository.getByField("email", email);
        user.setInvitationToken(token);
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getId() != id) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_DELETE);
        }
        userRepository.delete(id);
    }

}
