package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Rank;

import java.util.List;

public interface RankService {

    Rank getById(int id);

    Rank getByRank(String rank);

    List<Rank> getAll();

    void create(Rank rank);

    void update(Rank rank);
}
