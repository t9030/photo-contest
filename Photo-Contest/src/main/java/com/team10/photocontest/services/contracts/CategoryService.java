package com.team10.photocontest.services.contracts;

import com.team10.photocontest.models.Category;
import com.team10.photocontest.models.Comment;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    Category getByCategory(String categoryName);

    void create(Category category);

    void update(Category category);

    void delete(int id);
}
