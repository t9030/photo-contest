package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.Role;
import com.team10.photocontest.repositories.contracts.RoleRepository;
import com.team10.photocontest.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role getById(int id) {
        return roleRepository.getById(id);
    }

    @Override
    public Role getByName(String role) {
        return roleRepository.getByField("role", role);
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public void create(Role role) {
        boolean duplicateRole = true;

        try {
            roleRepository.getByField("role", role.getRole());
        } catch (EntityNotFoundException e) {
            duplicateRole = false;
        }
        if (duplicateRole) {
            throw new DuplicateEntityException("Role", "role", role.getRole());
        }

        roleRepository.create(role);
    }

    @Override
    public void update(Role role) {
        boolean duplicateRole = true;

        try {
            roleRepository.getByField("role", role.getRole());
        } catch (EntityNotFoundException e) {
            duplicateRole = false;
        }
        if (duplicateRole) {
            throw new DuplicateEntityException("Role", "role", role.getRole());
        }

        roleRepository.update(role);
    }
}
