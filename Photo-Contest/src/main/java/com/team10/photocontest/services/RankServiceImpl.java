package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.Rank;
import com.team10.photocontest.repositories.contracts.RankRepository;
import com.team10.photocontest.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;

    @Autowired
    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    @Override
    public Rank getById(int id) {
        return rankRepository.getById(id);
    }

    @Override
    public Rank getByRank(String rank) {
        return rankRepository.getByField("rank", rank);
    }

    @Override
    public List<Rank> getAll() {
        return rankRepository.getAll();
    }

    @Override
    public void create(Rank rank) {
        boolean duplicateRank = true;
        try {
            rankRepository.getByField("rank", rank.getRank());
        } catch (EntityNotFoundException e) {
            duplicateRank = false;
        }
        if (duplicateRank) {
            throw new DuplicateEntityException("Rank", "rank", rank.getRank());
        }

        rankRepository.create(rank);
    }

    @Override
    public void update(Rank rank) {
        boolean duplicateRank = true;

        try {
            rankRepository.getByField("rank", rank.getRank());
        } catch (EntityNotFoundException e) {
            duplicateRank = false;
        }
        if (duplicateRank) {
            throw new DuplicateEntityException("Rank", "rank", rank.getRank());
        }

        rankRepository.update(rank);
    }
}
