package com.team10.photocontest.models.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class VoteDTO {
    @NotNull(message = "You should rate the Photo!")
    @Positive
    private int points = 1;

    @NotEmpty(message = "You should leave a comment")
    private String content;

    private boolean wrongCategory;

    private int authorId;

    private int submissionId;

    public VoteDTO() {
    }
    public VoteDTO(int points, String content, boolean wrongCategory, int authorId, int submissionId) {
        this.points = points;
        this.content = content;
        this.wrongCategory = wrongCategory;
        this.authorId = authorId;
        this.submissionId = submissionId;
    }
    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(int submissionId) {
        this.submissionId = submissionId;
    }

    public boolean isWrongCategory() {
        return wrongCategory;
    }

    public void setWrongCategory(boolean wrongCategory) {
        this.wrongCategory = wrongCategory;
    }
}
