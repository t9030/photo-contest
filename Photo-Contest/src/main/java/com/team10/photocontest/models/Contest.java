package com.team10.photocontest.models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "is_open")
    private boolean isOpen;

    @Column(name="phase_1")
    private LocalDateTime endPhaseI;

    @Column(name = "phase_2")
    private LocalDateTime endPhaseII;

    @JoinColumn(name = "photo_id")
    @OneToOne
    private Photo photo;

    @Column(name = "is_scored")
    private boolean isScored;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<PhotoSubmission> photoSubmissions;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contestants",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> contestants;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "juries",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jury;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "invited_users",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> invited;

    public Contest() {
    }

    public Contest(int id,
                   String title,
                   Category category,
                   boolean isOpen,
                   LocalDateTime endPhaseI,
                   LocalDateTime endPhaseII,
                   Photo photo) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.isOpen = isOpen;
        this.endPhaseI = endPhaseI;
        this.endPhaseII = endPhaseII;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public LocalDateTime getEndPhaseI() {
        return endPhaseI;
    }

    public void setEndPhaseI(LocalDateTime endPhaseI) {
        this.endPhaseI = endPhaseI;
    }

    public LocalDateTime getEndPhaseII() {
        return endPhaseII;
    }

    public void setEndPhaseII(LocalDateTime endPhaseII) {
        this.endPhaseII = endPhaseII;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public boolean isScored() {
        return isScored;
    }

    public void setScored(boolean scored) {
        isScored = scored;
    }

    public Set<PhotoSubmission> getPhotoSubmissions() {
        return photoSubmissions;
    }

    public void setPhotoSubmissions(Set<PhotoSubmission> photos) {
        this.photoSubmissions = photos;
    }

    public Set<User> getJury() {
        return jury;
    }

    public Set<User> getContestants() {
        return contestants;
    }

    public void setContestants(Set<User> contestants) {
        this.contestants = contestants;
    }

    public void setJury(Set<User> jury) {
        this.jury = jury;
    }

    public Set<User> getInvited() {
        return invited;
    }

    public void setInvited(Set<User> invited) {
        this.invited = invited;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return id == contest.id
                && isOpen == contest.isOpen
                && Objects.equals(title, contest.title)
                && Objects.equals(category, contest.category)
                && Objects.equals(endPhaseI, contest.endPhaseI)
                && Objects.equals(endPhaseII, contest.endPhaseII)
                && Objects.equals(photo, contest.photo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, category, isOpen,
                endPhaseI, endPhaseI,  photo);
    }
}
