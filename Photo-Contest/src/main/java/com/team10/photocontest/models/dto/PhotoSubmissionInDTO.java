package com.team10.photocontest.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PhotoSubmissionInDTO {

    @NotNull(message = "Title can't be empty")
    @Size(min = 16, max = 64, message = "Title should be between 16 and 64 symbols")
    private String title;

    @NotNull(message = "Story can't be empty")
    @Size(min = 32, max = 8192, message = "Story should be between 32 and 8192 symbols")
    private String story;

    @NotNull(message = "Photo can't be empty")
    private MultipartFile photo;

    @NotNull(message = "Author can't be empty")
    private int authorId;

    public PhotoSubmissionInDTO() {
    }

    public PhotoSubmissionInDTO(String title, String story, MultipartFile photo, int authorId) {
        this.title = title;
        this.story = story;
        this.photo = photo;
        this.authorId = authorId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
