package com.team10.photocontest.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestCreateDTO {

    @NotNull(message = "Title can't be empty")
    @Size(min = 3, max = 64, message = "Title should be between 3 and 64 symbols")
    private String title;

    private int categoryId;

    @NotNull(message = "you should choose one of the options")
    private boolean isOpen;

    @NotNull(message = "You should choose end date and time for Phase I")
    private String endPhaseIDate;

    @NotNull(message = "You should choose an hour between 1 and 24")
    @Min(1)
    @Max(24)
    private int phaseIIDuration;

    private String urlCoverPhoto;

    private String existingUrlCoverPhoto;

    @NotNull(message = "You should choose a cover photo")
    private MultipartFile photo;

    public ContestCreateDTO() {
    }

    public ContestCreateDTO(String title, int categoryId, boolean isOpen,
                            String endPhaseIDate,  int phaseIIDuration, MultipartFile photo) {
        this.title = title;
        this.categoryId = categoryId;
        this.isOpen = isOpen;
        this.endPhaseIDate = endPhaseIDate;
        this.phaseIIDuration = phaseIIDuration;
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean open) {
        isOpen = open;
    }

    public String getEndPhaseIDate() {
        return endPhaseIDate;
    }

    public void setEndPhaseIDate(String endPhaseIDate) {
        this.endPhaseIDate = endPhaseIDate;
    }

    public void setPhaseIIDuration(int phaseIIDuration) {
        this.phaseIIDuration = phaseIIDuration;
    }

    public int getPhaseIIDuration() {
        return phaseIIDuration;
    }

    public String getUrlCoverPhoto() {
        return urlCoverPhoto;
    }

    public void setUrlCoverPhoto(String urlCoverPhoto) {
        this.urlCoverPhoto = urlCoverPhoto;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getExistingUrlCoverPhoto() {
        return existingUrlCoverPhoto;
    }

    public void setExistingUrlCoverPhoto(String existingUrlCoverPhoto) {
        this.existingUrlCoverPhoto = existingUrlCoverPhoto;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }
}
