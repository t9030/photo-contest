package com.team10.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int id;

    @Column(name = "submission_id")
    private int submissionId;

    @OneToOne
    @JoinColumn(name = "jury_id")
    private User author;

    @Column(name = "content")
    private String content;

    @Column(name = "points")
    private int points;

    public Comment() {
    }

    public Comment(int id, int submissionId, User author, String content, int points) {
        this.id = id;
        this.submissionId = submissionId;
        this.author = author;
        this.content = content;
        this.points = points;
    }

    public Comment(User author, String content, int points) {
        this.author = author;
        this.content = content;
        this.points = points;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(int submissionId) {
        this.submissionId = submissionId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id
                && author.equals(comment.author)
                && content.equals(comment.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, content);
    }
}
