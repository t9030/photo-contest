package com.team10.photocontest.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class UserRegisterDTO {

    @Size(min = 2, max = 40, message = "Username should be between 2 and 40 symbols")
    private String username;

    @Email(message = "Email should be valid")
    @Size(max = 100, message = "Email should have maximum 100 symbols")
    private String email;

    @Size(min = 2, max = 32, message = "First name should be between 2 and 32 symbols")
    private String firstName;

    @Size(min = 2, max = 32, message = "Last name should be between 2 and 32 symbols")
    private String lastName;

    @Size(min = 8, message = "Password should be at least 8 symbols")
    private String password;

    private String confirmPassword;

    private String urlAvatar;

    private MultipartFile multipartAvatar;

    public UserRegisterDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public MultipartFile getMultipartAvatar() {
        return multipartAvatar;
    }

    public void setMultipartAvatar(MultipartFile avatar) {
        this.multipartAvatar = avatar;
    }
}
