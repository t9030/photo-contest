package com.team10.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ranks")
public class Rank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rank_id")
    private int id;

    @Column(name = "rank")
    private String rank;

    public Rank() {
    }

    public Rank(int id, String rank) {
        this.id = id;
        this.rank = rank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rank rank1 = (Rank) o;
        return id == rank1.id && rank.equals(rank1.rank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rank);
    }
}
