package com.team10.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int id;

    @OneToOne
    @JoinColumn(name = "type_id")
    private ImageType type;

    @Column(name = "content")
    private String content;

    public Photo() {
    }

    public Photo(String content) {
        this.content = content;
    }

    public Photo(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public Photo(byte[] photo) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
