package com.team10.photocontest.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "image_types")
public class ImageType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private int id;

    @Column(name = "type")
    private String type;

    public ImageType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageType imageType = (ImageType) o;
        return id == imageType.id && type.equals(imageType.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }
}
