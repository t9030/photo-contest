package com.team10.photocontest.models;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "photo_submissions")
public class PhotoSubmission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_submission_id")
    private int id;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @OneToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @OneToOne
    @JoinColumn(name = "author_id")
    private User author;

    @Column(name = "score")
    private int score;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "submission_id")
    private Set<Comment> comments;

    public PhotoSubmission() {
    }

    public PhotoSubmission(int id, int contestId, String title, String story, Photo photo,
                           User author, int score, Set<Comment> comments) {
        this.id = id;
        this.contestId = contestId;
        this.title = title;
        this.story = story;
        this.photo = photo;
        this.author = author;
        this.score = score;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhotoSubmission that = (PhotoSubmission) o;
        return id == that.id
                && title.equals(that.title)
                && story.equals(that.story)
                && author.equals(that.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, story, author);
    }
}
