package com.team10.photocontest.models.dto;

public class InviteDTO {

    private String emails;

    public InviteDTO() {
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }
}
