package com.team10.photocontest.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ContestUpdateDTO {

    @Positive(message = "Id should be positive")
    private int id;

    @NotNull(message = "Name can't be empty")
    @Size(min = 16, max = 64, message = "Name should be between 16 and 64 symbols")
    private String name;



}
