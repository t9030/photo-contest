package com.team10.photocontest.repositories;

import com.team10.photocontest.models.Contest;
import com.team10.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {

    private static final String UNSUPPORTED_SORT =
            "Sort should have at least one or maximum two params divided by _ symbol!";

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
    }

    @Override
    public List<Contest> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "from Contest where title like :searchTerm or category.category like :searchTerm",
                    Contest.class);
            query.setParameter("searchTerm", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<String> category, Optional<Boolean> open,
                                Optional<String> phase, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Contest as contest ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            title.ifPresent(value -> {
                filter.add(" contest.title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            category.ifPresent(value -> {
                filter.add("contest.category.category = :category");
                queryParams.put("category", value);
            });

            open.ifPresent(value -> {
                filter.add(" isOpen = :open");
                queryParams.put("open", value);
            });

            phase.ifPresent(value -> {
                switch (value){
                    case "phaseI":
                        filter.add(" contest.endPhaseI > :date ");
                        break;
                    case "phaseII":
                        filter.add(" contest.endPhaseII > :date and contest.endPhaseI < :date");
                        break;
                    case "finished":
                        filter.add(" contest.endPhaseII < :date ");
                        break;
                }
                queryParams.put("date", LocalDateTime.now());
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value.trim()));
            });

            Query<Contest> queryList = session.createQuery(queryString.toString(), Contest.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length < 1) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }

        switch (params[0]) {
            case "title":
                queryString.append(" contest.title ");
                break;
            case "phaseI":
                queryString.append(" contest.phase_1 ");
                break;
            case "phaseII":
                queryString.append(" contest.phase_2 ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }

        return queryString.toString();
    }

}
