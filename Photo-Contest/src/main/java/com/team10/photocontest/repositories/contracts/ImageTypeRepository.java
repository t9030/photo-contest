package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.ImageType;

public interface ImageTypeRepository extends BaseCRUDRepository<ImageType>{
}
