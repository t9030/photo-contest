package com.team10.photocontest.repositories;

import com.team10.photocontest.models.Rank;
import com.team10.photocontest.repositories.contracts.RankRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RankRepositoryImpl extends AbstractCRUDRepository<Rank> implements RankRepository {
    public RankRepositoryImpl(SessionFactory sessionFactory) {
        super(Rank.class, sessionFactory);
    }
}
