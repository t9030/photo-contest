package com.team10.photocontest.repositories;

import com.team10.photocontest.models.Comment;
import com.team10.photocontest.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class CommentRepositoryImpl extends AbstractCRUDRepository<Comment> implements CommentRepository {

    private static final String UNSUPPORTED_SORT =
            "Sort should have at least one or maximum two params divided by _ symbol!";

    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        super(Comment.class, sessionFactory);
    }

    @Override
    public List<Comment> filter(Optional<String> authorUsername, Optional<Integer> submissionId, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Comment as comment");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            authorUsername.ifPresent(value -> {
                filter.add(" comment.author.username like :authorUsername ");
                queryParams.put("authorUsername", "%" + value + "%");
            });

            submissionId.ifPresent(value -> {
                filter.add(" comment.submissionId = :submissionId ");
                queryParams.put("submissionId", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value.trim()));
            });

            Query<Comment> queryList = session.createQuery(queryString.toString(), Comment.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length < 1) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }
        switch (params[0]) {
            case "id":
                queryString.append(" comment.id ");
                break;
            case "title":
                queryString.append(" comment.title ");
                break;
            case "authorUsername":
                queryString.append(" comment.author.username ");
                break;
        }
        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }
        return queryString.toString();
    }
}
