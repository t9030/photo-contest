package com.team10.photocontest.repositories;

import com.team10.photocontest.models.Role;
import com.team10.photocontest.repositories.contracts.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends AbstractCRUDRepository<Role> implements RoleRepository {
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(Role.class, sessionFactory);
    }
}
