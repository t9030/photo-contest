package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.Photo;

import java.util.List;
import java.util.Optional;

public interface PhotoRepository extends BaseReadRepository<Photo>{

    List<Photo> search(Optional<String> search);

    void create(Photo photo);

}
