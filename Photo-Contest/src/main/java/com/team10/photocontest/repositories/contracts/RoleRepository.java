package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.Role;

public interface RoleRepository extends BaseCRUDRepository<Role>{
}
