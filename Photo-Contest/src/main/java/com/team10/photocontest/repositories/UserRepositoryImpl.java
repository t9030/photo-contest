package com.team10.photocontest.repositories;

import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.query.Query;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    private static final String UNSUPPORTED_SORT =
            "Sort should have at least one or maximum two params divided by _ symbol!";

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where username like :name or firstName like :name or lastName like :name ",
                    User.class);
            query.setParameter("name", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<User> filter(Optional<String> username, Optional<String> roleName,
                             Optional<String> rankName, Optional<String> sort) {
        try(Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from User as user ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            username.ifPresent(value -> {
                filter.add(" user.username = :username ");
                queryParams.put("username", value);
            });

            roleName.ifPresent(value -> {
                filter.add(" user.role.role = :roleName ");
                queryParams.put("roleName", value);
            });

            rankName.ifPresent(value -> {
                filter.add(" user.rank.rank = :rankName ");
                queryParams.put("rankName", value);
            });

            if (!filter.isEmpty()){
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value.trim()));
            });

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);

            return queryList.list().stream()
                    .filter(user -> !user.isDeleted())
                    .collect(Collectors.toList());
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length < 1) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }

        switch (params[0]) {
            case "username":
                queryString.append(" user.username ");
                break;
            case "points":
                queryString.append(" user.points ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }

        return queryString.toString();
    }

    @Override
    public String topPlace(int id){
        try (Session session = sessionFactory.openSession()){

            User user = getById(id);
            BigInteger countAll = (BigInteger) session.createNativeQuery(
                    "select count(*) from users where is_deleted = false").getSingleResult();
            NativeQuery query = session.createNativeQuery(
                    "select count(*) from users where is_deleted = false and points > :better");
            query.setParameter("better", user.getPoints());
            BigInteger better = (BigInteger) query.getSingleResult();
            int intValue = (better.intValue() * 100)/countAll.intValue();

            return intValue < 10 ? "0" + intValue : String.valueOf(intValue);
        }
    }

    @Override
    public void delete(int id){
        User toDelete = getById(id);
        toDelete.setDeleted(true);
        toDelete.setUsername("DeletedUser" + toDelete.getId());
        toDelete.setEmail("DeletedUserEmail" + toDelete.getId());
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(toDelete);
            session.getTransaction().commit();
        }
    }
}
