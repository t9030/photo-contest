package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.Rank;

public interface RankRepository extends BaseCRUDRepository<Rank>{
}
