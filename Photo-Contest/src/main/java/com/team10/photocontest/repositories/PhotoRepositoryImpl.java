package com.team10.photocontest.repositories;

import com.team10.photocontest.models.Photo;
import com.team10.photocontest.repositories.contracts.PhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PhotoRepositoryImpl extends AbstractReadRepository<Photo> implements PhotoRepository {

    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(Photo.class, sessionFactory);
    }

    @Override
    public List<Photo> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Photo> query = session.createQuery("from Photo where " +
                    "type.type = :name or content= :name", Photo.class);
            query.setParameter("name", search.get());

            return query.list();
        }
    }

    @Override
    public void create(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(photo);
            session.getTransaction().commit();
        }
    }
}
