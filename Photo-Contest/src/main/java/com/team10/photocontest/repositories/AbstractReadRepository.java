package com.team10.photocontest.repositories;

import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

import static java.lang.String.format;

public abstract class AbstractReadRepository<T> implements BaseReadRepository<T> {
    private final Class<T> clazz;
    protected final SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    /**
     * Retrieves an entity from the database that has a <code>field</code> equal to <code>value</code>.
     * <br/>
     * Example: <code>getByField("id", 1, Parcel.class)</code>
     * will execute the following HQL: <code>from Parcel where id = 1;</code>
     *
     * @param name  the name of the field
     * @param value the value of the field
     * @return an entity that matches the given criteria
     */
    public <V> T getByField(String name, V value) {
        final String query;
        if(clazz.getSimpleName().equals("User")){
            query = format(" from %s where %s = :value and is_deleted = false", clazz.getSimpleName(), name);
        }
        else {
            query = format(" from %s where %s = :value", clazz.getSimpleName(), name);
        }

        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery(query, clazz)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), name, String.valueOf(value)));
        }
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            String query;
            if (clazz.getSimpleName().equals("User")){
                query = format(" from %s where is_deleted = false", clazz.getName());
            }
            else {
                query = format(" from %s ", clazz.getName());
            }
            return session.createQuery(query, clazz).list();
        }
    }

}
