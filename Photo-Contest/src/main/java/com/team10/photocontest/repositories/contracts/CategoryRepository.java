package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.Category;
import com.team10.photocontest.models.Contest;

public interface CategoryRepository extends BaseCRUDRepository<Category> {


}
