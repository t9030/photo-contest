package com.team10.photocontest.repositories;

import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.repositories.contracts.PhotoSubmissionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class PhotoSubmissionRepositoryImpl
        extends AbstractCRUDRepository<PhotoSubmission>
        implements PhotoSubmissionRepository {

    private static final String UNSUPPORTED_SORT =
            "Sort should have at least one or maximum two params divided by _ symbol!";

    public PhotoSubmissionRepositoryImpl(SessionFactory sessionFactory) {
        super(PhotoSubmission.class, sessionFactory);
    }

    @Override
    public List<PhotoSubmission> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<PhotoSubmission> query = session.createQuery(
                    " from PhotoSubmission where title like :name " +
                            "or author.username like :name or story like :name",
                    PhotoSubmission.class);
            query.setParameter("name", "%" + search.get() + "%");

            return query.list();
        }
    }

    @Override
    public List<PhotoSubmission> filter(Optional<String> authorUsername, Optional<Integer> contestId,
                                        Optional<String> contestCategory, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from PhotoSubmission as submission");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            authorUsername.ifPresent(value -> {
                filter.add(" submission.author.username = :authorUsername ");
                queryParams.put("authorUsername",  value);
            });

            contestId.ifPresent(value -> {
                filter.add(" submission.contestId = :contestId ");
                queryParams.put("contestId", value);
            });

            contestCategory.ifPresent(value -> {
                filter.add(" submission.contest.category.category = :contestCategory ");
                queryParams.put("contestCategory", value);
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value.trim()));
            });

            Query<PhotoSubmission> queryList = session.createQuery(queryString.toString(), PhotoSubmission.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty() || params.length < 1) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }
        switch (params[0]) {
            case "id":
                queryString.append(" submission.id ");
                break;
            case "title":
                queryString.append(" submission.title ");
                break;
            case "score":
                queryString.append(" submission.score ");
                break;
        }
        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException(UNSUPPORTED_SORT);
        }
        return queryString.toString();
    }
}
