package com.team10.photocontest.repositories;

import com.team10.photocontest.models.Category;
import com.team10.photocontest.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<Category> implements CategoryRepository {

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
    }

}
