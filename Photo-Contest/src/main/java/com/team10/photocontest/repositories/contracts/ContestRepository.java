package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.PhotoSubmission;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {

    List<Contest> search(Optional<String> search);

    List<Contest> filter(Optional<String> title, Optional<String> category,
                         Optional<Boolean> open, Optional<String> phase,
                         Optional<String> sort);

}
