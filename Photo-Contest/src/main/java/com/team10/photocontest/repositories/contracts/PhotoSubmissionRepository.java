package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.PhotoSubmission;

import java.util.List;
import java.util.Optional;

public interface PhotoSubmissionRepository extends BaseCRUDRepository<PhotoSubmission>{

    List<PhotoSubmission> search(Optional<String> search);

    List<PhotoSubmission> filter(Optional<String> authorUsername, Optional<Integer> contestId,
                                 Optional<String> contestCategory, Optional<String> sort);
}
