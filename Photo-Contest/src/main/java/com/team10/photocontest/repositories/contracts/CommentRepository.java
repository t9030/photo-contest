package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends BaseCRUDRepository<Comment>{
    List<Comment> filter(Optional<String> authorUsername, Optional<Integer> submissionId, Optional<String> sort);
}
