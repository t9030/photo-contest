package com.team10.photocontest.repositories;

import com.team10.photocontest.models.ImageType;
import com.team10.photocontest.repositories.contracts.ImageTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ImageTypeRepositoryImpl extends AbstractCRUDRepository<ImageType> implements ImageTypeRepository {
    public ImageTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(ImageType.class, sessionFactory);
    }
}
