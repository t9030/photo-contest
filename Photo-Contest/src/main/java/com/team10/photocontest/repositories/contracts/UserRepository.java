package com.team10.photocontest.repositories.contracts;

import com.team10.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User>{

    List<User> search(Optional<String> search);

    List<User> filter(Optional<String> username, Optional<String> roleName,
                      Optional<String> rankName, Optional<String> sort);

    String topPlace(int id);
}
