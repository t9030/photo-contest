package com.team10.photocontest.utils.mappers;

import com.team10.photocontest.controllers.helpers.CloudinaryHelper;
import com.team10.photocontest.models.Comment;
import com.team10.photocontest.models.Photo;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.dto.PhotoSubmissionInDTO;
import com.team10.photocontest.models.dto.PhotoSubmissionOutDTO;
import com.team10.photocontest.models.dto.VoteDTO;
import com.team10.photocontest.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class SubmissionMapper {

    private final UserService userService;
    private final PhotoService photoService;
    private final CloudinaryHelper cloudinaryHelper;
    private final ContestService contestService;
    private final PhotoSubmissionService photoSubmissionService;
    private final CommentService commentService;

    @Autowired
    public SubmissionMapper(UserService userService, PhotoService photoService,
                            CloudinaryHelper cloudinaryHelper, ContestService contestService,
                            PhotoSubmissionService photoSubmissionService, CommentService commentService) {
        this.userService = userService;
        this.photoService = photoService;
        this.cloudinaryHelper = cloudinaryHelper;
        this.contestService = contestService;
        this.photoSubmissionService = photoSubmissionService;
        this.commentService = commentService;
    }

    public PhotoSubmission DTOtoObject(PhotoSubmissionInDTO submissionDTO, int contestId) throws IOException {
        PhotoSubmission photoSubmission = new PhotoSubmission();
        photoSubmission.setContestId(contestId);
        photoSubmission.setTitle(submissionDTO.getTitle());
        photoSubmission.setStory(submissionDTO.getStory());
        photoSubmission.setAuthor(userService.getById(submissionDTO.getAuthorId()));
        Photo photo = cloudinaryHelper.uploadCloudinary(submissionDTO.getPhoto(), "Photo");
        photoSubmission.setPhoto(photoService.getById(photo.getId()));
        photoSubmission.setComments(new HashSet<>());
        return photoSubmission;
    }

    public Comment DTOtoObject(VoteDTO voteDTO){
        Comment comment = new Comment();
        comment.setSubmissionId(voteDTO.getSubmissionId());
        comment.setContent(voteDTO.getContent());
        comment.setPoints(voteDTO.getPoints());
        comment.setAuthor(userService.getById(voteDTO.getAuthorId()));
        return comment;
    }

    public PhotoSubmissionOutDTO ObjectToDTO(PhotoSubmission photoSubmission){
        PhotoSubmissionOutDTO submissionOut = new PhotoSubmissionOutDTO();
        submissionOut.setId(photoSubmission.getId());
        submissionOut.setContestTitle(contestService.getById(photoSubmission.getContestId()).getTitle());
        submissionOut.setContestId(photoSubmission.getContestId());
        submissionOut.setAuthorUsername(photoSubmission.getAuthor().getUsername());
        submissionOut.setTitle(photoSubmission.getTitle());
        submissionOut.setStory(photoSubmission.getStory());
        submissionOut.setPhoto(photoSubmission.getPhoto().getContent());
        submissionOut.setScore(photoSubmission.getScore());
        submissionOut.setPlace(calculatePlace(photoSubmission));
        submissionOut.setComments(commentService.filter(Optional.empty(),
                Optional.of(photoSubmission.getId()), Optional.of("id_desc")));
        return submissionOut;
    }

    public void voteObject(VoteDTO voteDTO){
        if (voteDTO.isWrongCategory()){
            photoSubmissionService.rate(voteDTO.getSubmissionId(),  0);
            Comment comment = DTOtoObject(voteDTO);
            comment.setPoints(0);
            comment.setContent("*WRONG CATEGORY*\n"+voteDTO.getContent());
            photoSubmissionService.comment(voteDTO.getSubmissionId(), comment);
        }else{
            Comment comment = DTOtoObject(voteDTO);
            photoSubmissionService.comment(voteDTO.getSubmissionId(), comment);
            photoSubmissionService.rate(voteDTO.getSubmissionId(), voteDTO.getPoints());
        }
    }

    private int calculatePlace(PhotoSubmission submission){
        List<PhotoSubmission> submissions = photoSubmissionService
                .filter(Optional.empty(), Optional.of(submission.getContestId()),
                        Optional.empty(), Optional.of("score_desc"));
        int place = 1;
        int i = 0;
        while (submission.getScore() < submissions.get(i).getScore() && i < submissions.size() - 1){
            if (submission.getScore() != submissions.get(i).getScore()){
                place++;
            }
            i++;
        }
        return place;
    }
}
