package com.team10.photocontest.utils.mappers;

import com.team10.photocontest.controllers.helpers.CloudinaryHelper;
import com.team10.photocontest.models.Photo;
import com.team10.photocontest.models.User;
import com.team10.photocontest.models.dto.UserOutDTO;
import com.team10.photocontest.models.dto.UserRegisterDTO;
import com.team10.photocontest.models.dto.UserUpdateDTO;
import com.team10.photocontest.repositories.contracts.RankRepository;
import com.team10.photocontest.repositories.contracts.RoleRepository;
import com.team10.photocontest.services.contracts.PhotoService;
import com.team10.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class UserMapper {

    private final RoleRepository roleRepository;
    private final RankRepository rankRepository;
    private final CloudinaryHelper cloudinaryHelper;
    private final PhotoService photoService;
    private final UserService userService;

    @Autowired
    public UserMapper(RoleRepository roleRepository, RankRepository rankRepository,
                      CloudinaryHelper cloudinaryHelper, PhotoService photoService,
                      UserService userService) {
        this.roleRepository = roleRepository;
        this.rankRepository = rankRepository;
        this.cloudinaryHelper = cloudinaryHelper;
        this.photoService = photoService;
        this.userService = userService;
    }

    public User DTOtoObject(UserRegisterDTO userRegisterDTO) throws IOException {
        User user = new User();
        user.setUsername(userRegisterDTO.getUsername());
        user.setEmail(userRegisterDTO.getEmail());
        user.setFirstName(userRegisterDTO.getFirstName());
        user.setLastName(userRegisterDTO.getLastName());
        user.setPassword(userRegisterDTO.getPassword());
        if (userRegisterDTO.getMultipartAvatar() != null && !userRegisterDTO.getMultipartAvatar().isEmpty()){
            Photo avatar = cloudinaryHelper.uploadCloudinary(userRegisterDTO.getMultipartAvatar(), "Avatar");
            user.setAvatar(avatar);
        }else {
            user.setAvatar(photoService.getByContent(userRegisterDTO.getUrlAvatar()));
        }
        user.setPoints(0);
        user.setRank(rankRepository.getByField("rank", "Junkie"));
        user.setRole(roleRepository.getByField("role", "User"));
        user.setDeleted(false);
        return user;
    }

    public UserOutDTO ObjectToDTO(User user){
        UserOutDTO userOutDTO = new UserOutDTO();
        userOutDTO.setId(user.getId());
        userOutDTO.setUsername(user.getUsername());
        userOutDTO.setFirstName(user.getFirstName());
        userOutDTO.setLastName(user.getLastName());
        userOutDTO.setAvatar(user.getAvatar().getContent());
        userOutDTO.setEmail(user.getEmail());
        userOutDTO.setPoints(user.getPoints());
        userOutDTO.setRank(user.getRank().getRank());
        userOutDTO.setRole(user.getRole().getRole());
        return userOutDTO;
    }

    public UserUpdateDTO objectToUpdateDto(User user){
        UserUpdateDTO userUpdateDTO = new UserUpdateDTO();
        userUpdateDTO.setId(user.getId());
        userUpdateDTO.setFirstName(user.getFirstName());
        userUpdateDTO.setLastName(user.getLastName());
        userUpdateDTO.setUrlAvatar(user.getAvatar().getContent());
        return userUpdateDTO;
    }

    public User UpdateDTOtoUser(UserUpdateDTO userDTO) throws IOException {
        User user = userService.getById(userDTO.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getMultipartAvatar() != null && !userDTO.getMultipartAvatar().isEmpty()){
            Photo avatar = cloudinaryHelper.uploadCloudinary(userDTO.getMultipartAvatar(), "Avatar");
            user.setAvatar(avatar);
        }else if (userDTO.getUrlAvatar() != null && !userDTO.getUrlAvatar().isEmpty()){
            user.setAvatar(photoService.getByContent(userDTO.getUrlAvatar()));
        }

        if (!userDTO.getNewPassword().isEmpty()){
            user.setPassword(userDTO.getNewPassword());
        }
        return user;
    }
}
