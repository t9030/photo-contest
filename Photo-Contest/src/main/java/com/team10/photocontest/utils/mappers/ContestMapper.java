package com.team10.photocontest.utils.mappers;

import com.team10.photocontest.controllers.helpers.CloudinaryHelper;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.Photo;
import com.team10.photocontest.models.dto.ContestCreateDTO;
import com.team10.photocontest.services.contracts.CategoryService;
import com.team10.photocontest.services.contracts.ImageTypeService;
import com.team10.photocontest.services.contracts.PhotoService;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Optional;

@Component
public class ContestMapper {

    private final CloudinaryHelper cloudinaryHelper;
    private final PhotoService photoService;
    private final CategoryService categoryService;
    private final ImageTypeService imageTypeService;

    public ContestMapper(CloudinaryHelper cloudinaryHelper, PhotoService photoService,
                         CategoryService categoryService, ImageTypeService imageTypeService) {
        this.cloudinaryHelper = cloudinaryHelper;
        this.photoService = photoService;
        this.categoryService = categoryService;
        this.imageTypeService = imageTypeService;
    }

    public Contest dtoToObject(ContestCreateDTO createDTO) throws IOException, ParseException {
        Contest contest = new Contest();
        contest.setTitle(createDTO.getTitle());
        contest.setCategory(categoryService.getById(createDTO.getCategoryId()));
        contest.setOpen(createDTO.getIsOpen());

        String str = twelveTo24(createDTO.getEndPhaseIDate());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        contest.setEndPhaseI(dateTime);
        contest.setEndPhaseII(contest.getEndPhaseI().plusHours(createDTO.getPhaseIIDuration()));
        contest.setScored(false);
        contest.setPhotoSubmissions(new HashSet<>());

        if (createDTO.getPhoto() != null && !createDTO.getPhoto().isEmpty()){
            Photo photo = cloudinaryHelper.uploadCloudinary(createDTO.getPhoto(), "Cover Photo");
            contest.setPhoto(photo);
        }else if(createDTO.getExistingUrlCoverPhoto() != null &&
                !photoService.getAll(Optional.of(createDTO.getExistingUrlCoverPhoto())).isEmpty()){
            contest.setPhoto(photoService.getByContent(createDTO.getExistingUrlCoverPhoto()));
        }else{
            Photo photo = new Photo(createDTO.getUrlCoverPhoto());
            photo.setType(imageTypeService.getByType("Cover Photo"));
            photoService.create(photo);
            contest.setPhoto(photo);
        }
        return contest;
    }

    private String twelveTo24(String endPhaseIDate) {
        String[] parts = endPhaseIDate.split(" ");
        parts[0] = parts[0].replace("/", "-");
        if (parts[2].equals("PM")) {
            String oldHours = parts[1].split(":")[0];
            String minutes = parts[1].split(":")[1];
            String newHours = oldHours.equals("12") ? "00" : String.valueOf(Integer.parseInt(oldHours) + 12);
            parts[1] = String.format("%s:%s", newHours, minutes);
        }
        else {
            String hour = String.valueOf(parts[1].charAt(0));
            String newHour = Integer.parseInt(hour) == 1 ? hour : "0" + hour;
            parts[1] = parts[1].replaceFirst(String.valueOf(parts[1].charAt(0)), newHour);
        }
        return parts[0] + " " + parts[1];
    }
}
