package com.team10.photocontest.services;

import com.team10.photocontest.models.Comment;
import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.CommentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.team10.photocontest.Helpers.createMockComment;
import static com.team10.photocontest.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class CommentServiceImplTests {

    @Mock
    CommentRepository commentRepository;

    @InjectMocks
    CommentServiceImpl commentService;

    @Test
    public void getAll_should_callRepository(){
        //Arrange
        Mockito.when(commentRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        commentService.getAll();

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_ReturnComment_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        Mockito.when(commentRepository.getById(mockComment.getId()))
                .thenReturn(mockComment);

        //Act
        Comment result = commentService.getById(mockComment.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockComment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockComment.getAuthor(), result.getAuthor()),
                () -> Assertions.assertEquals(mockComment.getContent(), result.getContent())
        );

    }

    @Test
    public void filter_Should_callRepository(){
        //Arrange, Act
        commentService.filter(Optional.empty(), Optional.empty(), Optional.empty());

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void createComment_should_callRepository(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);

        //Act
        commentService.create( mockComment);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .create( mockComment);
    }

    @Test
    public void update_Should_callRepository(){
        //Arrange
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);

        //Act
        commentService.update(mockComment);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .update(mockComment);
    }

    @Test
    public void delete_Should_callRepository(){
        //Arrange
        User mockCommentAuthor = createMockUser();
        Comment mockComment = createMockComment(mockCommentAuthor);

        //Act
        commentService.delete(mockComment.getId());

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .delete(mockComment.getId());
    }

}
