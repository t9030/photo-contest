package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Rank;
import com.team10.photocontest.models.Role;
import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.RankRepository;
import com.team10.photocontest.repositories.contracts.RoleRepository;
import com.team10.photocontest.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.team10.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @Mock
    RankRepository rankRepository;

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(userRepository.search(Optional.empty()))
                .thenReturn(new ArrayList<>());

        //Act
        userService.getAll(Optional.empty());

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .search(Optional.empty());
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getById(mockUser.getId())).thenReturn(mockUser);

        //Act
        User result = userService.getById(mockUser.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void getByUsername_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("username",mockUser.getUsername())).thenReturn(mockUser);

        //Act
        User result = userService.getByUsername(mockUser.getUsername());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void getByEmail_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("email",mockUser.getEmail())).thenReturn(mockUser);

        //Act
        User result = userService.getByEmail(mockUser.getEmail());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void getByResetPasswordToken_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("resetPasswordToken", mockUser.getResetPasswordToken()))
                .thenReturn(mockUser);

        //Act
        User result = userService.getByResetPasswordToken(mockUser.getResetPasswordToken());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void getByInvitationToken_Should_ReturnUser_When_MatchExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("invitation_token", mockUser.getInvitationToken()))
                .thenReturn(mockUser);

        //Act
        User result = userService.getByInvitationToken(mockUser.getInvitationToken());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.isDeleted(), result.isDeleted())
        );
    }

    @Test
    public void topPlace_should_callRepository() {
        //Arrange
        User mockUser = createMockUser();

        //Act
        userService.topPlace(Mockito.anyInt());

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .topPlace(Mockito.anyInt());
    }

    @Test
    public void filter_Should_ReturnUsers_When_MatchesExist() {
        //Arrange
        User mockUser = createMockUser();
        User mockUser2 = createMockUser();
        Mockito.when(userRepository.filter(Optional.empty(), Optional.of("User"),
                        Optional.empty(), Optional.empty()))
                .thenReturn(List.of(mockUser, mockUser2));

        //Act
        userService.filter(Optional.empty(), Optional.of("User"),
                Optional.empty(), Optional.empty());

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.of("User"),
                        Optional.empty(), Optional.empty());
    }

    @Test
    public void create_Should_Throw_When_UsernameAlreadyExists(){
        //Arrange
        User mockUser = createMockUser();

        Mockito.when(userRepository.getByField("username", mockUser.getUsername())).thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void create_Should_Throw_When_EmailAlreadyExists(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("username", mockUser.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "username", mockUser.getUsername()));
        Mockito.when(userRepository.getByField("email", mockUser.getEmail())).thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void createUser_should_callRepository_When_UserIsNotDuplicate(){
        //Arrange
        User mockUser = createMockUser();

        Mockito.when(userRepository.getByField("username", mockUser.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "username", mockUser.getUsername()));
        Mockito.when(userRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(new EntityNotFoundException("User", "email", mockUser.getEmail()));

        //Act
        userService.create(mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).create(mockUser);
    }

    @Test
    public void update_should_Throw_When_UserIsNotOwner(){
        //Arrange
        User mockUser = createMockUser();
        User mockAnotherUser = createMockUser();
        mockAnotherUser.setId(2);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(mockUser, mockAnotherUser));
    }

    @Test
    public void update_should_callRepository_When_UserIsOwner(){
        //Arrange
        User mockUser = createMockUser();

        //Act
        userService.update(mockUser, mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void update_should_changeRankToEnthusiast(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setPoints(51);
        Mockito.when(rankRepository.getByField("rank", "Enthusiast"))
                .thenReturn(new Rank(2, "Enthusiast"));

        //Act
        userService.update(mockUser, mockUser);

        //Assert
        Assertions.assertEquals("Enthusiast", mockUser.getRank().getRank());
    }

    @Test
    public void update_should_changeRankToMaster(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setPoints(151);
        Mockito.when(rankRepository.getByField("rank", "Master"))
                .thenReturn(new Rank(2, "Master"));

        //Act
        userService.update(mockUser, mockUser);

        //Assert
        Assertions.assertEquals("Master", mockUser.getRank().getRank());
    }

    @Test
    public void update_should_changeRankToDictator(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setPoints(1001);
        Mockito.when(rankRepository.getByField("rank", "Wise and Benevolent Photo Dictator"))
                .thenReturn(new Rank(2, "Wise and Benevolent Photo Dictator"));

        //Act
        userService.update(mockUser, mockUser);

        //Assert
        Assertions.assertEquals("Wise and Benevolent Photo Dictator", mockUser.getRank().getRank());
    }

    @Test
    public void promote_should_Throw_When_UserIsNotOrganizer(){
        //Arrange
        User mockUser = createMockUser();
        User mockUpdater = createMockUser();
        mockUpdater.setRole(new Role(2, "User"));
        Mockito.when(userRepository.getById(mockUser.getId())).thenReturn(mockUser);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.promote(mockUser.getId(), mockUpdater));
    }

    @Test
    public void promote_should_ChangeRole_When_UserIsOrganizer(){
        //Arrange
        User mockUser = createMockUser();
        User mockUpdater = createMockUser();
        Mockito.when(userRepository.getById(mockUser.getId())).thenReturn(mockUser);
        Mockito.when(roleRepository.getByField("role", "Organizer")).thenReturn(mockUpdater.getRole());

        //Act
        userService.promote(mockUser.getId(), mockUpdater);

        //Assert
        Assertions.assertEquals("Organizer", mockUser.getRole().getRole());
    }

    @Test
    public void updateResetPasswordToken_should_ChangeToken(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("email", mockUser.getEmail())).thenReturn(mockUser);

        //Act
        userService.updateResetPasswordToken("mockResetPasswordToken", mockUser.getEmail());

        //Assert
        Assertions.assertEquals("mockResetPasswordToken", mockUser.getResetPasswordToken());
    }

    @Test
    public void updateInvitationToken_should_ChangeToken(){
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userRepository.getByField("email", mockUser.getEmail())).thenReturn(mockUser);

        //Act
        userService.updateInvitationToken("mockInvitationToken", mockUser.getEmail());

        //Assert
        Assertions.assertEquals("mockInvitationToken", mockUser.getInvitationToken());
    }

    @Test
    public void updatePassword_should_ChangePassword_And_Make_TokenNull(){
        //Arrange
        User mockUser = createMockUser();
        mockUser.setResetPasswordToken("mockResetPasswordToken");

        //Act
        userService.updatePassword(mockUser, "mockPassword");

        //Assert
        Assertions.assertAll(
                ()-> Assertions.assertEquals("mockPassword", mockUser.getPassword()),
                ()-> Assertions.assertNull(mockUser.getResetPasswordToken())
        );

    }



    @Test
    public void delete_should_Throw_When_UserIsNotOwner(){
        //Arrange
        User mockUser = createMockUser();
        User mockAnotherUser = createMockUser();
        mockAnotherUser.setId(2);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(mockUser.getId(), mockAnotherUser));
    }

    @Test
    public void delete_should_callRepository_When_UserIsOwner(){
        //Arrange
        User mockUser = createMockUser();

        //Act
        userService.delete(mockUser.getId(), mockUser);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).delete(mockUser.getId());
    }

}
