package com.team10.photocontest.services;

import com.team10.photocontest.models.Photo;
import com.team10.photocontest.repositories.contracts.PhotoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.team10.photocontest.Helpers.createMockPhoto;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceImplTests {

    @Mock
    PhotoRepository photoRepository;

    @InjectMocks
    PhotoServiceImpl photoService;

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(photoRepository.search(Optional.empty())).thenReturn(new ArrayList<>());

        //Act
        photoService.getAll(Optional.empty());

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1)).search(Optional.empty());
    }

    @Test
    public void getById_Should_ReturnCategory_When_MatchExists(){
        //Arrange
        Photo mockPhoto = createMockPhoto();
        Mockito.when(photoRepository.getById(mockPhoto.getId())).thenReturn(mockPhoto);

        //Act
        Photo result = photoService.getById(mockPhoto.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPhoto.getId(), result.getId()),
                () -> Assertions.assertEquals(mockPhoto.getContent(), result.getContent())
        );
    }

    @Test
    public void getByContent_Should_ReturnPhoto_When_MatchExists(){
        //Arrange
        Photo mockPhoto = createMockPhoto();
        Mockito.when(photoRepository.getByField("content", mockPhoto.getContent()))
                .thenReturn(mockPhoto);

        //Act
        Photo result = photoService.getByContent(mockPhoto.getContent());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockPhoto.getId(), result.getId()),
                () -> Assertions.assertEquals(mockPhoto.getContent(), result.getContent())
        );
    }

    @Test
    public void create_should_callRepository() {
        //Arrange
        Photo mockPhoto = createMockPhoto();

        //Act
        photoService.create(mockPhoto);

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1)).create(mockPhoto);
    }
}
