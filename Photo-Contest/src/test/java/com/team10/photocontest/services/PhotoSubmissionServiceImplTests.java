package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.*;
import com.team10.photocontest.repositories.contracts.CommentRepository;
import com.team10.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.team10.photocontest.services.contracts.ContestService;
import com.team10.photocontest.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.team10.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class PhotoSubmissionServiceImplTests {

    @Mock
    PhotoSubmissionRepository photoSubmissionRepository;
    @Mock
    ContestService contestService;
    @Mock
    UserService userService;
    @Mock
    CommentRepository commentRepository;

    @InjectMocks
    PhotoSubmissionServiceImpl photoSubmissionService;

    @Test
    public void getById_Should_ReturnPost_When_MatchExists(){
        //Arrange
        PhotoSubmission mockSubmission = createMockSubmission(Mockito.mock(User.class));
        Mockito.when(photoSubmissionRepository.getById(mockSubmission.getId())).thenReturn(mockSubmission);

        //Act
        PhotoSubmission result = photoSubmissionService.getById(mockSubmission.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockSubmission.getId(), result.getId()),
                () -> Assertions.assertEquals(mockSubmission.getAuthor(), result.getAuthor()),
                () -> Assertions.assertEquals(mockSubmission.getContestId(), result.getContestId())
        );
    }

    @Test
    public void getAll_should_callRepository(){
        //Arrange, Act
        photoSubmissionService.getAll(Optional.empty());

        //Assert
        Mockito.verify(photoSubmissionRepository, Mockito.times(1))
                .search(Optional.empty());
    }

    @Test
    public void filter_Should_CallRepository() {
        // Arrange
        Mockito.when(photoSubmissionRepository.filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        photoSubmissionService.filter(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty());

        // Assert
        Mockito.verify(photoSubmissionRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty());

    }

    @Test
    public void create_Should_AddSubmission_ToContest() {
        //Arrange
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);
        Mockito.when(userService.getByUsername(mockAuthor.getUsername())).thenReturn(mockAuthor);

        //Act
        photoSubmissionService.create(mockSubmission, mockContest.getId());

        //Assert
        Assertions.assertEquals(1, mockContest.getPhotoSubmissions().size());
    }

    @Test
    public void create_Should_AddUser_ToContestants() {
        //Arrange
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);
        Mockito.when(userService.getByUsername(mockAuthor.getUsername())).thenReturn(mockAuthor);

        //Act
        photoSubmissionService.create(mockSubmission, mockContest.getId());

        //Assert
        Assertions.assertEquals(1, mockContest.getContestants().size());
    }

    @Test
    public void create_Should_Give_User_A_Point_ForJoining() {
        //Arrange
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        PhotoSubmission mockSubmission = createMockSubmission(mockAuthor);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);
        Mockito.when(userService.getByUsername(mockAuthor.getUsername())).thenReturn(mockAuthor);

        //Act
        photoSubmissionService.create(mockSubmission, mockContest.getId());

        //Assert
        Assertions.assertEquals(1, mockAuthor.getPoints());
    }

    @Test
    public void create_Should_RemoveUser_FromInvited() {
        //Arrange
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        Set<User> invited = mockContest.getInvited();
        invited.add(mockAuthor);
        mockContest.setInvited(invited);
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);
        Mockito.when(userService.getByUsername(mockAuthor.getUsername())).thenReturn(mockAuthor);

        //Act
        photoSubmissionService.create(mockSubmission, mockContest.getId());

        //Assert
        Assertions.assertEquals(0, mockContest.getInvited().size());
    }

    @Test
    public void create_Should_GiveUser_ThreePoints_IfInvited() {
        //Arrange
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        Set<User> invited = mockContest.getInvited();
        invited.add(mockAuthor);
        mockContest.setInvited(invited);
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);
        Mockito.when(userService.getByUsername(mockAuthor.getUsername())).thenReturn(mockAuthor);

        //Act
        photoSubmissionService.create(mockSubmission, mockContest.getId());

        //Assert
        Assertions.assertEquals(3, mockAuthor.getPoints());
    }

    @Test
    public void create_Should_CallRepository(){
        // Arrange
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);
        Mockito.when(userService.getByUsername(mockAuthor.getUsername())).thenReturn(mockAuthor);

        // Act
        photoSubmissionService.create(mockSubmission, mockContest.getId());

        // Assert
        Mockito.verify(photoSubmissionRepository, Mockito.times(1))
                .create(mockSubmission);
    }

    @Test
    public void comment_should_callCommentRepository(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        PhotoSubmission mockSubmission =  createMockSubmission(mockUser);
        Mockito.when(photoSubmissionRepository.getById(Mockito.anyInt())).thenReturn(mockSubmission);

        //Act
        photoSubmissionService.comment(Mockito.anyInt(), mockComment);

        //Assert
        Mockito.verify(commentRepository, Mockito.times(1))
                .create(mockComment);
    }

    @Test
    public void comment_should_addCommentToSubmission(){
        //Arrange
        User mockUser = createMockUser();
        Comment mockComment = createMockComment(mockUser);
        PhotoSubmission mockSubmission =  createMockSubmission(mockUser);
        Mockito.when(photoSubmissionRepository.getById(Mockito.anyInt())).thenReturn(mockSubmission);

        //Act
        photoSubmissionService.comment(Mockito.anyInt(), mockComment);

        //Assert
        Assertions.assertEquals(1, mockSubmission.getComments().size());
    }

    @Test
    public void comment_should_callRepository(){
        //Arrange
        Comment mockComment = createMockComment(Mockito.mock(User.class));
        PhotoSubmission mockSubmission =  createMockSubmission(Mockito.mock(User.class));
        Mockito.when(photoSubmissionRepository.getById(Mockito.anyInt())).thenReturn(mockSubmission);

        //Act
        photoSubmissionService.comment(Mockito.anyInt(), mockComment);

        //Assert
        Mockito.verify(photoSubmissionRepository, Mockito.times(1))
                .update(mockSubmission);
    }

    @Test
    public void getJuryComment_should_returnComment(){
        //Arrange
        User mockJury = createMockUser();
        Comment mockComment = createMockComment(mockJury);
        PhotoSubmission mockSubmission =  createMockSubmission(Mockito.mock(User.class));
        Set<Comment> comments = mockSubmission.getComments();
        comments.add(mockComment);
        mockSubmission.setComments(comments);
        Mockito.when(photoSubmissionRepository.getById(Mockito.anyInt())).thenReturn(mockSubmission);

        //Act
        photoSubmissionService.getJuryComment(mockSubmission.getId(), mockJury.getId());

        //Assert
        Assertions.assertEquals(mockJury.getId(),
                photoSubmissionService.getJuryComment(mockSubmission.getId(), mockJury.getId()).getAuthor().getId());
    }

    @Test
    public void rate_should_addPointsToSubmission(){
        //Arrange
        PhotoSubmission mockSubmission =  createMockSubmission(Mockito.mock(User.class));
        Mockito.when(photoSubmissionRepository.getById(Mockito.anyInt())).thenReturn(mockSubmission);

        //Act
        photoSubmissionService.rate(mockSubmission.getId(), 10);

        //Assert
        Assertions.assertEquals(10, mockSubmission.getScore());
    }

    @Test
    public void rate_should_callRepository(){
        //Arrange
        PhotoSubmission mockSubmission =  createMockSubmission(Mockito.mock(User.class));
        Mockito.when(photoSubmissionRepository.getById(Mockito.anyInt())).thenReturn(mockSubmission);

        //Act
        photoSubmissionService.rate(mockSubmission.getId(), 10);

        //Assert
        Mockito.verify(photoSubmissionRepository, Mockito.times(1))
                .update(mockSubmission);
    }

    @Test
    public void delete_Should_Throw_IfUser_IsNotJury(){
        // Arrange
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        mockAuthor.setRole(new Role(1, "User"));
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(photoSubmissionRepository.getById(mockSubmission.getId())).thenReturn(mockSubmission);
        Mockito.when(contestService.getById(Mockito.anyInt())).thenReturn(mockContest);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoSubmissionService.delete(mockSubmission.getId(), mockUser));
    }

    @Test
    public void delete_Should_RemoveUser_FormContestants(){
        //Arrange
        User mockJury = createMockUser();
        Contest mockContest = createMockContest();
        User mockAuthor = createMockUser();
        User mockAuthor2 = createMockUser();
        mockAuthor2.setId(2);
        mockContest.setJury(new HashSet<>(List.of(mockJury)));
        mockContest.setContestants(new HashSet<>(List.of(mockAuthor, mockAuthor2)));
        PhotoSubmission mockSubmission =  createMockSubmission(mockAuthor);
        Mockito.when(photoSubmissionService.getById(mockSubmission.getId())).thenReturn(mockSubmission);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);

        //Act
        photoSubmissionService.delete(mockSubmission.getId(), mockJury);

        //Assert
        Assertions.assertEquals(1, mockContest.getContestants().size());
    }

    @Test
    public void delete_Should_CallRepository(){
        //Arrange
        Contest mockContest = createMockContest();
        User mockJury = createMockUser();
        Set<User> jury = mockContest.getJury();
        jury.add(mockJury);
        mockContest.setJury(jury);
        PhotoSubmission mockSubmission = createMockSubmission(Mockito.mock(User.class));
        Mockito.when(photoSubmissionService.getById(mockSubmission.getId())).thenReturn(mockSubmission);
        Mockito.when(contestService.getById(mockContest.getId())).thenReturn(mockContest);

        //Act
        photoSubmissionService.delete(mockSubmission.getId(), mockJury);

        // Assert
        Mockito.verify(photoSubmissionRepository, Mockito.times(1))
                .delete(mockSubmission.getId());
    }

}
