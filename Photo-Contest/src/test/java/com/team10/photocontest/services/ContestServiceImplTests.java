package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.exceptions.UnauthorizedOperationException;
import com.team10.photocontest.models.Contest;
import com.team10.photocontest.models.PhotoSubmission;
import com.team10.photocontest.models.Role;
import com.team10.photocontest.models.User;
import com.team10.photocontest.repositories.contracts.ContestRepository;
import com.team10.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.team10.photocontest.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.team10.photocontest.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository contestRepository;
    @Mock
    UserService userService;
    @Mock
    PhotoSubmissionRepository photoSubmissionRepository;

    @InjectMocks
    ContestServiceImpl contestService;


    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(contestRepository.search(Optional.empty()))
                .thenReturn(new ArrayList<>());

        //Act
        contestService.getAll(Optional.empty());

        //Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .search(Optional.empty());
    }

    @Test
    public void getById_Should_ReturnContest_When_MatchExists() {
        //Arrange
        Contest mockContest = createMockContest();
        Mockito.when(contestRepository.getById(mockContest.getId())).thenReturn(mockContest);

        //Act
        Contest result = contestService.getById(mockContest.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockContest.getId(), result.getId()),
                () -> Assertions.assertEquals(mockContest.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockContest.getPhoto(), result.getPhoto())
        );
    }

    @Test
    public void scoring_Should_Give_HigherPoints_If_PlacesNotShared(){
        //Arrange
        Role mockRole = createMockRole();
        User mockUser1 = createMockUser(mockRole, 1);
        User mockUser2 = createMockUser(mockRole, 2);
        User mockUser3 = createMockUser(mockRole, 3);
        User mockUser4 = createMockUser(mockRole, 4);
        Contest mockContest = createUnscoredContest(mockUser1, mockUser2, mockUser3, mockUser4,
                List.of(10, 15, 20, 25));
        Mockito.when(contestRepository.getById(mockContest.getId())).thenReturn(mockContest);
        //Act
        contestService.scoring(mockContest.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(0, mockUser1.getPoints()),
                () -> Assertions.assertEquals(20, mockUser2.getPoints()),
                () -> Assertions.assertEquals(35, mockUser3.getPoints()),
                () -> Assertions.assertEquals(50, mockUser4.getPoints())
        );
    }

    @Test
    public void scoring_Should_Give_LowerPoints_If_PlacesShared(){
        //Arrange
        Role mockRole = createMockRole();
        User mockUser1 = createMockUser(mockRole, 1);
        User mockUser2 = createMockUser(mockRole, 2);
        User mockUser3 = createMockUser(mockRole, 3);
        User mockUser4 = createMockUser(mockRole, 4);
        Contest mockContest = createUnscoredContest(mockUser1, mockUser2, mockUser3, mockUser4,
                List.of(15, 15, 25, 25));
        Mockito.when(contestRepository.getById(mockContest.getId())).thenReturn(mockContest);

        //Act
        contestService.scoring(mockContest.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(25, mockUser1.getPoints()),
                () -> Assertions.assertEquals(25, mockUser2.getPoints()),
                () -> Assertions.assertEquals(40, mockUser3.getPoints()),
                () -> Assertions.assertEquals(40, mockUser4.getPoints())
        );

    }

    @Test
    public void scoring_Should_Give_75Points_If_PlacesOne_IsDoubleTo_PlaceTwo(){
        //Arrange
        Role mockRole = createMockRole();
        User mockUser1 = createMockUser(mockRole, 1);
        User mockUser2 = createMockUser(mockRole, 2);
        User mockUser3 = createMockUser(mockRole, 3);
        User mockUser4 = createMockUser(mockRole, 4);
        Contest mockContest = createUnscoredContest(mockUser1, mockUser2, mockUser3, mockUser4,
                List.of(15, 15, 25, 50));
        Mockito.when(contestRepository.getById(mockContest.getId())).thenReturn(mockContest);

        //Act
        contestService.scoring(mockContest.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(10, mockUser1.getPoints()),
                () -> Assertions.assertEquals(10, mockUser2.getPoints()),
                () -> Assertions.assertEquals(35, mockUser3.getPoints()),
                () -> Assertions.assertEquals(75, mockUser4.getPoints())
        );

    }

    @Test
    public void filter_Should_CallRepository() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(contestRepository.filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        contestService.filter(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), mockUser);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty());
    }

    @Test
    public void filter_ShouldFilter_whenOpen() {
        // Arrange
        Contest mockOpenContest = createMockContest();
        Contest mockCloseContest = createMockContest();
        mockOpenContest.setOpen(true);
        User mockUser = createMockUser();
        Mockito.when(contestRepository.filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(List.of(mockCloseContest, mockOpenContest));

        //Act, Assert
        Assertions.assertEquals(1, contestService.filter(Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), mockUser).size());
    }

    @Test
    public void create_Should_CallRepository() {
        // Arrange
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();
        User mockJury1 = createMockUser();
        User mockJury2 = createMockUser();
        User mockJury3 = createMockUser();
        Mockito.when(contestRepository.getByField("title", mockContest.getTitle()))
                .thenThrow(new EntityNotFoundException("Contest", "title", "title"));
        Mockito.when(userService.filter(Optional.empty(),
                        Optional.of("Organizer"), Optional.empty(), Optional.empty()))
                .thenReturn(List.of(mockJury1, mockJury2, mockJury3));

        // Act
        contestService.create(mockContest, mockUser);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .create(mockContest);
    }

    @Test
    public void create_Should_Throw_When_TitleAlreadyExists() {
        //Arrange
        User mockUser = createMockUser();
        Contest mockContest = createMockContest();
        Mockito.when(contestRepository.getByField("title", mockContest.getTitle())).thenReturn(mockContest);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> contestService.create(mockContest, mockUser));
    }

    @Test
    public void create_shouldThrow_whenNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        mockUser.setRole(new Role(3, "User"));
        Contest mockContest = createMockContest();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.create(mockContest, mockUser));
    }

    @Test
    public void getByTitle_Should_ReturnContest_When_MatchExists() {
        //Arrange
        Contest mockContest = createMockContest();
        Mockito.when(contestRepository.getByField("title", mockContest.getTitle())).thenReturn(mockContest);

        //Act
        Contest result = contestService.getByTitle(mockContest.getTitle());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockContest.getId(), result.getId()),
                () -> Assertions.assertEquals(mockContest.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockContest.getPhoto(), result.getPhoto())
        );
    }

    @Test
    public void update_Should_CallRepository() {
        //Arrange
        Contest mockContest = createMockContest();

        // Act
        contestService.update(mockContest);

        // Assert
        Mockito.verify(contestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    public void getPhase_ShouldReturn_PhaseI() {
        //Arrange
        Contest mockContest = createMockContest();

        //Act, Assert
        Assertions.assertEquals(contestService.getPhase(mockContest),
                "Contest is in Phase I - Photo Submissions");
    }

    @Test
    public void getPhase_ShouldReturn_PhaseII() {
        //Arrange
        Contest mockContest = createMockContest();
        mockContest.setEndPhaseI(LocalDateTime.now().minusHours(3));

        //Act, Assert
        Assertions.assertEquals(contestService.getPhase(mockContest),
                "Contest is in Phase II - Jury Votes");
    }

    @Test
    public void getPhase_ShouldReturn_Finished() {
        //Arrange
        Contest mockContest = createMockContest();
        mockContest.setEndPhaseI(LocalDateTime.now().minusHours(7));
        mockContest.setEndPhaseII(LocalDateTime.now().minusHours(2));

        //Act, Assert
        Assertions.assertEquals(contestService.getPhase(mockContest),
                "This contest is already finished");
    }

    @Test
    public void getDateTime_ShouldReturnEndPhaseI() {
        //Arrange
        Contest mockContest = createMockContest();

        //Act, Assert
        Assertions.assertEquals(contestService.getDateTime(mockContest), mockContest.getEndPhaseI());
    }

    @Test
    public void getDateTime_ShouldReturnEndPhaseII() {
        //Arrange
        Contest mockContest = createMockContest();
        mockContest.setEndPhaseI(LocalDateTime.now().minusHours(5));

        //Act, Assert
        Assertions.assertEquals(contestService.getDateTime(mockContest), mockContest.getEndPhaseII());
    }

    @Test
    public void addParticipant_ShouldAddJury() {
        //Arrange
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();
        contestService.addParticipant(mockContest, mockUser, "Jury");

        //Act, Assert
        Assertions.assertEquals(1, mockContest.getJury().size());
    }

    @Test
    public void addParticipant_ShouldAddContestant(){
        //Arrange
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();
        contestService.addParticipant(mockContest, mockUser, "Contestant");

        //Act, Assert
        Assertions.assertEquals(1, mockContest.getInvited().size());
    }

    @Test
    public void unvoted_should_return3(){
        Contest mockContest = createMockContest();
        PhotoSubmission submission = createMockSubmission(createMockUser());
        mockContest.setPhotoSubmissions(new HashSet<>(List.of(submission)));
contestService.unvoted(mockContest);
Assertions.assertEquals(3, submission.getScore() );
    }

}
