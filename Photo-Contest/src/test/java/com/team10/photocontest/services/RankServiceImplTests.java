package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.Rank;
import com.team10.photocontest.repositories.contracts.RankRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team10.photocontest.Helpers.createMockRank;

@ExtendWith(MockitoExtension.class)
public class RankServiceImplTests {

    @Mock
    RankRepository rankRepository;

    @InjectMocks
    RankServiceImpl rankService;

    @Test
    public void getById_Should_ReturnRank_When_MatchExists(){
        //Arrange
        Rank mockRank = createMockRank();
        Mockito.when(rankRepository.getById(mockRank.getId())).thenReturn(mockRank);

        //Act
        Rank result = rankService.getById(mockRank.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRank.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRank.getRank(), result.getRank())
        );
    }

    @Test
    public void getByName_Should_ReturnRank_When_MatchExists(){
        //Arrange
        Rank mockRank = createMockRank();
        Mockito.when(rankRepository.getByField("rank",mockRank.getRank())).thenReturn(mockRank);

        //Act
        Rank result = rankService.getByRank(mockRank.getRank());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRank.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRank.getRank(), result.getRank())
        );
    }

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(rankRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        rankService.getAll();

        //Assert
        Mockito.verify(rankRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void create_Should_Throw_When_RankNameAlreadyExists(){
        //Arrange
        Rank mockRank = createMockRank();

        Mockito.when(rankRepository.getByField("rank", mockRank.getRank())).thenReturn(mockRank);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> rankService.create(mockRank));
    }

    @Test
    public void create_should_callRepository_When_RankIsNotDuplicate(){
        //Arrange
        Rank mockRank = createMockRank();

        Mockito.when(rankRepository.getByField("rank", mockRank.getRank()))
                .thenThrow(new EntityNotFoundException("Rank", "rank", mockRank.getRank()));

        //Act
        rankService.create(mockRank);

        //Assert
        Mockito.verify(rankRepository, Mockito.times(1)).create(mockRank);
    }

    @Test
    public void update_Should_Throw_When_RankNameAlreadyExists(){
        //Arrange
        Rank mockRank = createMockRank();

        Mockito.when(rankRepository.getByField("rank", mockRank.getRank())).thenReturn(mockRank);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> rankService.update(mockRank));
    }

    @Test
    public void update_should_callRepository_When_RoleIsNotDuplicate(){
        //Arrange
        Rank mockRank = createMockRank();

        Mockito.when(rankRepository.getByField("rank", mockRank.getRank()))
                .thenThrow(new EntityNotFoundException("Rank", "rank", mockRank.getRank()));

        //Act
        rankService.update(mockRank);

        //Assert
        Mockito.verify(rankRepository, Mockito.times(1)).update(mockRank);
    }
}
