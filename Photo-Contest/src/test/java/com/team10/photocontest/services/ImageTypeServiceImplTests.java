package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.ImageType;
import com.team10.photocontest.repositories.contracts.ImageTypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team10.photocontest.Helpers.createMockImageType;

@ExtendWith(MockitoExtension.class)
public class ImageTypeServiceImplTests {

    @Mock
    ImageTypeRepository imageTypeRepository;

    @InjectMocks
    ImageTypeServiceImpl imageTypeService;

    @Test
    public void getById_Should_ReturnImageType_When_MatchExists(){
        //Arrange
        ImageType mockImageType = createMockImageType();
        Mockito.when(imageTypeRepository.getById(mockImageType.getId())).thenReturn(mockImageType);

        //Act
        ImageType result = imageTypeService.getById(mockImageType.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockImageType.getId(), result.getId()),
                () -> Assertions.assertEquals(mockImageType.getType(), result.getType())
        );
    }

    @Test
    public void getByName_Should_ReturnImageType_When_MatchExists(){
        //Arrange
        ImageType mockImageType = createMockImageType();
        Mockito.when(imageTypeRepository.getByField("type",mockImageType.getType())).thenReturn(mockImageType);

        //Act
        ImageType result = imageTypeService.getByType(mockImageType.getType());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockImageType.getId(), result.getId()),
                () -> Assertions.assertEquals(mockImageType.getType(), result.getType())
        );
    }

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(imageTypeRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        imageTypeService.getAll();

        //Assert
        Mockito.verify(imageTypeRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void create_Should_Throw_When_ImageTypeNameAlreadyExists(){
        //Arrange
        ImageType mockImageType = createMockImageType();

        Mockito.when(imageTypeRepository.getByField("type", mockImageType.getType())).thenReturn(mockImageType);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> imageTypeService.create(mockImageType));
    }

    @Test
    public void create_should_callRepository_When_ImageTypeIsNotDuplicate(){
        //Arrange
        ImageType mockImageType = createMockImageType();

        Mockito.when(imageTypeRepository.getByField("type", mockImageType.getType()))
                .thenThrow(new EntityNotFoundException("Type", "type", mockImageType.getType()));

        //Act
        imageTypeService.create(mockImageType);

        //Assert
        Mockito.verify(imageTypeRepository, Mockito.times(1)).create(mockImageType);
    }

    @Test
    public void update_Should_Throw_When_ImageTypeNameAlreadyExists(){
        //Arrange
        ImageType mockImageType = createMockImageType();

        Mockito.when(imageTypeRepository.getByField("type", mockImageType.getType())).thenReturn(mockImageType);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> imageTypeService.update(mockImageType));
    }

    @Test
    public void update_should_callRepository_When_ImageTypeIsNotDuplicate(){
        //Arrange
        ImageType mockImageType = createMockImageType();

        Mockito.when(imageTypeRepository.getByField("type", mockImageType.getType()))
                .thenThrow(new EntityNotFoundException("Type", "type", mockImageType.getType()));

        //Act
        imageTypeService.update(mockImageType);

        //Assert
        Mockito.verify(imageTypeRepository, Mockito.times(1)).update(mockImageType);
    }
}
