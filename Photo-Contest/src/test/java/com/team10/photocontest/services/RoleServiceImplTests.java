package com.team10.photocontest.services;

import com.team10.photocontest.exceptions.DuplicateEntityException;
import com.team10.photocontest.exceptions.EntityNotFoundException;
import com.team10.photocontest.models.Role;
import com.team10.photocontest.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team10.photocontest.Helpers.createMockRole;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    public void getById_Should_ReturnRole_When_MatchExists(){
        //Arrange
        Role mockRole = createMockRole();
        Mockito.when(roleRepository.getById(mockRole.getId())).thenReturn(mockRole);

        //Act
        Role result = roleService.getById(mockRole.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getRole(), result.getRole())
        );
    }

    @Test
    public void getByName_Should_ReturnRole_When_MatchExists(){
        //Arrange
        Role mockRole = createMockRole();
        Mockito.when(roleRepository.getByField("role",mockRole.getRole())).thenReturn(mockRole);

        //Act
        Role result = roleService.getByName(mockRole.getRole());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getRole(), result.getRole())
        );
    }

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(roleRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        roleService.getAll();

        //Assert
        Mockito.verify(roleRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void create_Should_Throw_When_RoleNameAlreadyExists(){
        //Arrange
        Role mockRole = createMockRole();

        Mockito.when(roleRepository.getByField("role", mockRole.getRole())).thenReturn(mockRole);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> roleService.create(mockRole));
    }

    @Test
    public void create_should_callRepository_When_RoleIsNotDuplicate(){
        //Arrange
        Role mockRole = createMockRole();

        Mockito.when(roleRepository.getByField("role", mockRole.getRole()))
                .thenThrow(new EntityNotFoundException("Role", "role", mockRole.getRole()));

        //Act
        roleService.create(mockRole);

        //Assert
        Mockito.verify(roleRepository, Mockito.times(1)).create(mockRole);
    }

    @Test
    public void update_Should_Throw_When_RoleNameAlreadyExists(){
        //Arrange
        Role mockRole = createMockRole();

        Mockito.when(roleRepository.getByField("role", mockRole.getRole())).thenReturn(mockRole);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> roleService.update(mockRole));
    }

    @Test
    public void update_should_callRepository_When_RoleIsNotDuplicate(){
        //Arrange
        Role mockRole = createMockRole();

        Mockito.when(roleRepository.getByField("role", mockRole.getRole()))
                .thenThrow(new EntityNotFoundException("Role", "role", mockRole.getRole()));

        //Act
        roleService.update(mockRole);

        //Assert
        Mockito.verify(roleRepository, Mockito.times(1)).update(mockRole);
    }
}
