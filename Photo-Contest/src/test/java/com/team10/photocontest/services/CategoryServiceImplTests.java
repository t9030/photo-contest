package com.team10.photocontest.services;

import com.team10.photocontest.models.Category;
import com.team10.photocontest.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team10.photocontest.Helpers.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository categoryRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(categoryRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        categoryService.getAll();

        //Assert
        Mockito.verify(categoryRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnCategory_When_MatchExists(){
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(categoryRepository.getById(mockCategory.getId())).thenReturn(mockCategory);

        //Act
        Category result = categoryService.getById(mockCategory.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getCategory(), result.getCategory())
        );
    }

    @Test
    public void getByCategory_Should_ReturnCategory_When_MatchExists(){
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(categoryRepository.getByField("category", mockCategory.getCategory()))
                .thenReturn(mockCategory);

        //Act
        Category result = categoryService.getByCategory(mockCategory.getCategory());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getCategory(), result.getCategory())
        );
    }

    @Test
    public void create_should_callRepository() {
        //Arrange
        Category mockCategory = createMockCategory();

        //Act
        categoryService.create(mockCategory);

        //Assert
        Mockito.verify(categoryRepository, Mockito.times(1)).create(mockCategory);
    }

    @Test
    public void update_should_callRepository() {
        //Arrange
        Category mockCategory = createMockCategory();

        //Act
        categoryService.update(mockCategory);

        //Assert
        Mockito.verify(categoryRepository, Mockito.times(1)).update(mockCategory);
    }

    @Test
    public void delete_should_callRepository() {
        //Arrange
        Category mockCategory = createMockCategory();

        //Act
        categoryService.delete(mockCategory.getId());

        //Assert
        Mockito.verify(categoryRepository, Mockito.times(1)).delete(mockCategory.getId());
    }
}
