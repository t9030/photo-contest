package com.team10.photocontest;

import com.team10.photocontest.models.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

public class Helpers {

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("MockUsername");
        mockUser.setEmail("MockEmail@email.com");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setPoints(0);
        mockUser.setRank(new Rank(1, "Junkie"));
        mockUser.setRole(new Role(3, "Organizer"));
        mockUser.setDeleted(false);
        return mockUser;
    }

    public static User createMockUser(Role role, int id) {
        var mockUser = createMockUser();
        mockUser.setId(id);
        mockUser.setRole(role);
        return mockUser;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("User");
        return mockRole;
    }

    public static Rank createMockRank() {
        var mockRank = new Rank();
        mockRank.setId(1);
        mockRank.setRank("Master");
        return mockRank;
    }

    public static ImageType createMockImageType() {
        var mockImageType = new ImageType();
        mockImageType.setId(1);
        mockImageType.setType("MockType");
        return mockImageType;
    }

    public static Contest createMockContest() {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("MockContest");
        mockContest.setCategory(createMockCategory());
        mockContest.setPhoto(createMockPhoto());
        mockContest.setEndPhaseI(LocalDateTime.now().plusHours(5));
        mockContest.setEndPhaseII(LocalDateTime.now().plusHours(10));
        mockContest.setPhoto(createMockPhoto());
        mockContest.setContestants(new HashSet<>());
        mockContest.setInvited(new HashSet<>());
        mockContest.setJury(new HashSet<>());
        mockContest.setPhotoSubmissions(new HashSet<>());
        return mockContest;
    }

    public static Contest createUnscoredContest(User mockUser1, User mockUser2, User mockUser3, User mockUser4,
                                                List<Integer> points){
        Contest mockContest = createMockContest();
        mockContest.setScored(false);
        mockContest.setEndPhaseII(LocalDateTime.now().minusHours(5));
        PhotoSubmission submission1 = createScoredSubmission(mockUser1, points.get(0));
        PhotoSubmission submission2 = createScoredSubmission(mockUser2, points.get(1));
        PhotoSubmission submission3 = createScoredSubmission(mockUser3, points.get(2));
        PhotoSubmission submission4 = createScoredSubmission(mockUser4, points.get(3));
        mockContest.setPhotoSubmissions(new HashSet<>(List.of(submission1, submission2, submission3, submission4)));
        return mockContest;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setCategory("MockCategory");
        return mockCategory;
    }

    public static Photo createMockPhoto() {
        var mockPhoto = new Photo();
        mockPhoto.setId(1);
        mockPhoto.setContent("https://images.unsplash.com/photo-1609607847926-da4702f01fef?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=393&q=80");
        mockPhoto.setType(createMockImageType());
        return mockPhoto;
    }

    public static Comment createMockComment(User mockUser) {
        var mockComment = new Comment();
        mockComment.setPoints(10);
        mockComment.setSubmissionId(1);
        mockComment.setId(1);
        mockComment.setAuthor(mockUser);
        mockComment.setContent("MockContent");
        return mockComment;
    }

    public static PhotoSubmission createMockSubmission(User mockUser) {
        var mockSubmission = new PhotoSubmission();
        mockSubmission.setId(1);
        mockSubmission.setScore(0);
        mockSubmission.setAuthor(mockUser);
        mockSubmission.setPhoto(createMockPhoto());
        mockSubmission.setTitle("Mock Title");
        mockSubmission.setComments(new HashSet<>());
        mockSubmission.setContestId(1);
        return mockSubmission;
    }

    public static PhotoSubmission createScoredSubmission(User mockUser, int score) {
        var mockSubmission = createMockSubmission(mockUser);
        mockSubmission.setComments(new HashSet<>(List.of(createMockComment(createMockUser()))));
        mockSubmission.setScore(score);
        return mockSubmission;
    }

}

