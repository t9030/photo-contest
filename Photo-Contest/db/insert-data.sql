-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.7-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table photo_contest.categories: ~17 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `category`) VALUES
                                                         (1, 'Default'),
                                                         (2, 'Animals'),
                                                         (3, 'Sport'),
                                                         (4, 'Portraits'),
                                                         (5, 'Cities'),
                                                         (6, 'Landscapes'),
                                                         (7, 'Hobby'),
                                                         (8, 'Sea'),
                                                         (9, 'Mountain'),
                                                         (10, 'Stars'),
                                                         (11, 'Sky'),
                                                         (12, 'Road'),
                                                         (13, 'Rivers'),
                                                         (14, 'Work'),
                                                         (15, 'Village'),
                                                         (16, 'Holidays'),
                                                         (17, 'Night Photography');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table photo_contest.comments: ~16 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`comment_id`, `content`, `points`, `submission_id`, `jury_id`) VALUES
                                                                                           (7, 'very good', 0, 10, 4),
                                                                                           (8, 'Nice light', 0, 12, 4),
                                                                                           (16, 'Lucky shot!', 0, 15, 8),
                                                                                           (17, 'It is a bit basic!', 5, 19, 4),
                                                                                           (18, 'Happy Easter to you too! :D', 5, 31, 8),
                                                                                           (19, 'Bad image quality', 3, 29, 8),
                                                                                           (20, 'Easter masterpiece', 8, 32, 8),
                                                                                           (21, 'Cuteness overload ^_^', 7, 30, 8),
                                                                                           (22, 'Not a bad idea', 5, 31, 4),
                                                                                           (23, 'Did yours win?', 5, 29, 4),
                                                                                           (24, 'Masterpiece', 10, 32, 4),
                                                                                           (25, 'Really cute', 6, 30, 4),
                                                                                           (26, 'I like the headline but that\'s about it.', 4, 31, 7),
                                                                                           (27, 'Fiercesome battle is obout to begin.', 4, 29, 7),
                                                                                           (28, 'Nice photography', 9, 32, 7),
                                                                                           (29, 'What are their names??', 6, 30, 7);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table photo_contest.contestants: ~29 rows (approximately)
/*!40000 ALTER TABLE `contestants` DISABLE KEYS */;
INSERT INTO `contestants` (`contest_id`, `user_id`) VALUES
                                                        (9, 1),
                                                        (9, 2),
                                                        (9, 3),
                                                        (9, 5),
                                                        (9, 6),
                                                        (14, 5),
                                                        (14, 1),
                                                        (15, 5),
                                                        (15, 9),
                                                        (21, 9),
                                                        (21, 5),
                                                        (21, 1),
                                                        (20, 1),
                                                        (20, 5),
                                                        (12, 5),
                                                        (17, 5),
                                                        (22, 1),
                                                        (12, 9),
                                                        (12, 1),
                                                        (15, 11),
                                                        (12, 11),
                                                        (23, 10),
                                                        (23, 11),
                                                        (23, 5),
                                                        (23, 1),
                                                        (24, 6),
                                                        (24, 5),
                                                        (24, 11),
                                                        (24, 9);
/*!40000 ALTER TABLE `contestants` ENABLE KEYS */;

-- Dumping data for table photo_contest.contests: ~15 rows (approximately)
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` (`contest_id`, `title`, `category_id`, `is_open`, `phase_1`, `phase_2`, `photo_id`, `is_scored`) VALUES
                                                                                                                            (9, 'Night City', 4, 1, '2022-04-14 12:44:00', '2022-04-15 00:44:00', 4, 0),
                                                                                                                            (10, 'Animals in the Snow', 2, 1, '2022-04-20 12:51:00', '2022-04-21 12:51:00', 16, 0),
                                                                                                                            (11, 'Domestic Birds', 2, 0, '2022-04-20 16:44:00', '2022-04-21 16:44:00', 18, 0),
                                                                                                                            (12, 'Pets', 2, 1, '2022-04-29 16:53:00', '2022-04-30 10:53:00', 19, 0),
                                                                                                                            (14, 'Selfies', 2, 1, '2022-04-20 19:51:00', '2022-04-21 12:51:00', 23, 0),
                                                                                                                            (15, 'Architecture', 5, 1, '2022-05-01 08:17:00', '2022-05-02 08:17:00', 28, 0),
                                                                                                                            (16, 'Moody Night', 5, 0, '2022-04-29 11:04:00', '2022-04-29 22:04:00', 35, 0),
                                                                                                                            (17, 'Photography', 3, 1, '2022-04-28 19:15:00', '2022-04-29 19:15:00', 36, 0),
                                                                                                                            (18, 'Cities', 5, 1, '2022-04-30 13:16:00', '2022-05-01 11:16:00', 37, 0),
                                                                                                                            (19, 'Cats', 2, 0, '2022-04-29 13:17:00', '2022-04-30 11:17:00', 38, 0),
                                                                                                                            (20, 'Cat', 2, 1, '2022-04-20 14:13:00', '2022-04-20 17:13:00', 39, 1),
                                                                                                                            (21, 'Dogs', 2, 1, '2022-04-19 11:19:00', '2022-04-21 14:19:00', 40, 1),
                                                                                                                            (22, 'Summer Storm', 6, 0, '2022-04-22 11:14:00', '2022-04-22 13:05:25', 50, 1),
                                                                                                                            (23, 'Easter', 16, 1, '2022-04-22 10:53:00', '2022-04-23 17:42:57', 64, 1),
                                                                                                                            (24, 'Lights', 17, 0, '2022-04-29 10:16:00', '2022-04-30 10:16:00', 70, 0);
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;

-- Dumping data for table photo_contest.image_types: ~4 rows (approximately)
/*!40000 ALTER TABLE `image_types` DISABLE KEYS */;
INSERT INTO `image_types` (`type_id`, `type`) VALUES
                                                  (3, 'Avatar'),
                                                  (1, 'Cover Photo'),
                                                  (4, 'Default Avatar'),
                                                  (2, 'Photo');
/*!40000 ALTER TABLE `image_types` ENABLE KEYS */;

-- Dumping data for table photo_contest.invited_users: ~20 rows (approximately)
/*!40000 ALTER TABLE `invited_users` DISABLE KEYS */;
INSERT INTO `invited_users` (`contest_id`, `user_id`) VALUES
                                                          (9, 1),
                                                          (9, 2),
                                                          (9, 3),
                                                          (9, 5),
                                                          (9, 6),
                                                          (10, 1),
                                                          (10, 2),
                                                          (10, 3),
                                                          (10, 6),
                                                          (10, 5),
                                                          (11, 1),
                                                          (11, 2),
                                                          (11, 3),
                                                          (11, 6),
                                                          (11, 5),
                                                          (12, 2),
                                                          (12, 3),
                                                          (12, 6),
                                                          (12, 5),
                                                          (22, 3);
/*!40000 ALTER TABLE `invited_users` ENABLE KEYS */;

-- Dumping data for table photo_contest.juries: ~44 rows (approximately)
/*!40000 ALTER TABLE `juries` DISABLE KEYS */;
INSERT INTO `juries` (`contest_id`, `user_id`) VALUES
                                                   (9, 4),
                                                   (10, 7),
                                                   (10, 8),
                                                   (10, 4),
                                                   (11, 7),
                                                   (11, 8),
                                                   (11, 4),
                                                   (12, 7),
                                                   (12, 8),
                                                   (12, 4),
                                                   (14, 7),
                                                   (14, 4),
                                                   (14, 8),
                                                   (15, 7),
                                                   (15, 4),
                                                   (15, 8),
                                                   (16, 7),
                                                   (16, 4),
                                                   (16, 8),
                                                   (17, 7),
                                                   (17, 4),
                                                   (17, 8),
                                                   (18, 7),
                                                   (18, 4),
                                                   (18, 8),
                                                   (19, 7),
                                                   (19, 4),
                                                   (19, 8),
                                                   (20, 7),
                                                   (20, 4),
                                                   (20, 8),
                                                   (21, 7),
                                                   (21, 4),
                                                   (21, 8),
                                                   (16, 3),
                                                   (22, 7),
                                                   (22, 4),
                                                   (22, 8),
                                                   (23, 7),
                                                   (23, 4),
                                                   (23, 8),
                                                   (24, 7),
                                                   (24, 4),
                                                   (24, 8);
/*!40000 ALTER TABLE `juries` ENABLE KEYS */;

-- Dumping data for table photo_contest.photos: ~60 rows (approximately)
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` (`photo_id`, `type_id`, `content`) VALUES
                                                            (1, 2, 'https://res.cloudinary.com/photocontest/image/upload/v1650363423/vmlywy9cqkdb2im15z5d.jpg'),
                                                            (2, 3, 'https://res.cloudinary.com/photocontest/image/upload/v1650456277/trevor-buntin-ZAB9w-eAeM0-unsplash_ua8xcv.jpg'),
                                                            (3, 3, 'http://res.cloudinary.com/photocontest/image/upload/v1649262662/sx1w7cxs9vlzpdeji3jm.jpg'),
                                                            (4, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650363423/vmlywy9cqkdb2im15z5d.jpg'),
                                                            (16, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650457618/jonatan-pie-xgTMSz6kegE-unsplash_icxjux.jpg'),
                                                            (17, 4, 'https://res.cloudinary.com/photocontest/image/upload/v1650040474/photo-1605725968485-09f0d1ad8f9a_my5ift.jpg'),
                                                            (18, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650457966/vidar-nordli-mathisen-dC-z4r8tr6U-unsplash_vyvsdp.jpg'),
                                                            (19, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650458137/jamie-street-MoDcnVRN5JU-unsplash_pzwagh.jpg'),
                                                            (20, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1649842946/lgmven1yalz7msudfy91.jpg'),
                                                            (21, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1649843014/lkfrlip5he0yrjrhnd9u.jpg'),
                                                            (23, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650209502/qqqiqnnzt9on5mn8qxvw.jpg'),
                                                            (24, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1649955540/vcpgimcxizjma963fkhw.jpg'),
                                                            (25, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1649956245/yu52xcnnhgweg2crurql.jpg'),
                                                            (26, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1649956415/j35rwvoao4fkqs36etgx.jpg'),
                                                            (27, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650028746/zdad9xyuqk3lx22dfton.jpg'),
                                                            (28, 1, 'http://res.cloudinary.com/photocontest/image/upload/v1650302332/yg8agn2abikywwy2j6np.jpg'),
                                                            (29, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650303074/i3kindoc8ejkgkagcvwp.jpg'),
                                                            (30, 4, 'https://res.cloudinary.com/photocontest/image/upload/v1650040476/photo-1592429141138-559164149da7_daipbs.jpg'),
                                                            (33, 4, 'https://res.cloudinary.com/photocontest/image/upload/v1650041246/photo-1623577284502-d65cdc6ba0b6_hab0b9.jpg'),
                                                            (34, 4, 'https://res.cloudinary.com/photocontest/image/upload/v1650041022/photo-1516035069371-29a1b244cc32_ajqzmx.jpg'),
                                                            (35, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650659697/photo-1514813621023-7a1e3fca8c1b_k8qj6a.jpg'),
                                                            (36, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650659824/photo-1606223016431-a39013f36710_w25yrm.jpg'),
                                                            (37, 1, 'http://res.cloudinary.com/photocontest/image/upload/v1650363423/vmlywy9cqkdb2im15z5d.jpg'),
                                                            (38, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650457775/agape-trn-H-XOmJhbM3s-unsplash_sbavvj.jpg'),
                                                            (39, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650659964/photo-1589883661923-6476cb0ae9f2_nbwncq.jpg'),
                                                            (40, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650660006/photo-1597633544424-4da83804df40_ty67f5.jpg'),
                                                            (41, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650377650/rs9pjnrirypifa8khgpv.jpg'),
                                                            (42, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650377923/qxzlwpwwwt5yqksqtghp.jpg'),
                                                            (43, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650378271/wh9zw5aswxwdnaf2c2nd.jpg'),
                                                            (44, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650378375/xrbqnms7qopo1zp3hw63.jpg'),
                                                            (45, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650379633/xnrtg66y8vrfiyzvo3j7.jpg'),
                                                            (46, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650379743/tszhconv4sgymsodnqmt.jpg'),
                                                            (47, 2, 'https://res.cloudinary.com/photocontest/image/upload/v1649666150/otzlwxybq6kvwsmmmjdy.jpg'),
                                                            (48, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650461596/ocwnzry0nrtat0zvr8tc.jpg'),
                                                            (49, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650528712/k1frybzadxx37vhw3ky2.jpg'),
                                                            (50, 1, 'https://res.cloudinary.com/photocontest/image/upload/v1650660100/photo-1500674425229-f692875b0ab7_wo9vis.jpg'),
                                                            (51, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650531368/fkqeuh89h2fkaqgyvl4w.jpg'),
                                                            (52, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650642266/j4t56n47uondbmenn1x1.jpg'),
                                                            (53, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650643001/s1fnjdsvxm5b7qngipjw.jpg'),
                                                            (54, 3, 'http://res.cloudinary.com/photocontest/image/upload/v1650644837/zfbgsbzsgn3ylhrmaxe6.jpg'),
                                                            (55, 3, 'http://res.cloudinary.com/photocontest/image/upload/v1650644920/icgk8xicukesnjmeeb7r.jpg'),
                                                            (56, 3, 'http://res.cloudinary.com/photocontest/image/upload/v1650645754/hnnny1ndwjol5wqcsgyy.jpg'),
                                                            (57, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650664805/nm57ugxtj741enmznwyp.jpg'),
                                                            (58, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650664993/fwskzueqe3axgiqx8hs1.jpg'),
                                                            (59, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650665448/yvfhfwcdsfwe7ticqynp.jpg'),
                                                            (60, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650667023/kwft02ifhehw783wc0lt.jpg'),
                                                            (61, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650668294/hczw12gwwlct8v1n3sgz.jpg'),
                                                            (62, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650668566/u8vkogl8dnlkst8iefpq.jpg'),
                                                            (63, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650711901/xqgq7bgg1vneuw5shlpl.jpg'),
                                                            (64, 1, 'http://res.cloudinary.com/photocontest/image/upload/v1650714858/aug490pwps2ojypcaoxp.jpg'),
                                                            (65, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650715268/k3ivabewyxvoajmj0k0r.jpg'),
                                                            (66, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650723270/py3fmtjd4jv1ay8r04lc.jpg'),
                                                            (67, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650723464/avr3ikif0jpr25uxuu8p.jpg'),
                                                            (68, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650724008/tdlxeekwm7bi90ent5rd.jpg'),
                                                            (69, 4, 'https://res.cloudinary.com/photocontest/image/upload/v1650041176/photo-1554080353-a576cf803bda_kaq0vg.jpg'),
                                                            (70, 1, 'http://res.cloudinary.com/photocontest/image/upload/v1650982663/ioethw1d0eocarnxaaaa.jpg'),
                                                            (71, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650983298/tybzuaf6e6e2zxmckrph.jpg'),
                                                            (72, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650983498/hx5dw04ipzbxqiahjhio.jpg'),
                                                            (74, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650984079/vuz4ndccaw2zkja5oq07.jpg'),
                                                            (75, 2, 'http://res.cloudinary.com/photocontest/image/upload/v1650984346/tmik3nqv4enyswv241kc.jpg');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;

-- Dumping data for table photo_contest.photo_submissions: ~22 rows (approximately)
/*!40000 ALTER TABLE `photo_submissions` DISABLE KEYS */;
INSERT INTO `photo_submissions` (`photo_submission_id`, `title`, `story`, `photo_id`, `score`, `author_id`, `contest_id`) VALUES
                                                                                                                              (10, 'kanal', 'village', 29, 0, 5, 15),
                                                                                                                              (11, 'night bridge', 'We were travelling last year...', 41, 0, 9, 15),
                                                                                                                              (12, 'my dog', 'Somthing something', 42, 30, 9, 21),
                                                                                                                              (13, 'two dogs', 'I found them on the road', 43, 20, 5, 21),
                                                                                                                              (14, 'my new dog', 'Happy', 44, 10, 1, 21),
                                                                                                                              (15, 'Buterfly', 'Beauty', 45, 10, 1, 20),
                                                                                                                              (16, 'Cat', 'Suny Day', 46, 0, 5, 20),
                                                                                                                              (17, 'Cat', 'I don\'t have dog, but I want to participate', 48, 0, 5, 12),
                                                                                                                              (18, 'Macro shooting', 'Me shooting plants', 49, 0, 5, 17),
                                                                                                                              (19, 'Sunflowers in the storm', 'Even in the storm, there is sunlight!', 51, 5, 1, 22),
                                                                                                                              (20, 'Macro', 'There is no strory', 52, 0, 9, 12),
                                                                                                                              (21, 'Innocent', 'Are you talking to me?!', 53, 0, 1, 12),
                                                                                                                              (27, 'City', 'Working laptop', 62, 0, 11, 15),
                                                                                                                              (28, 'Cute cat', 'I saw this while drinking my coffee!', 63, 0, 11, 12),
                                                                                                                              (29, 'Egg hunt', 'Moments before the absolute disaster. :D :D', 65, 12, 10, 23),
                                                                                                                              (30, 'Bunnies', 'What is Easter without the Easter Bunnies??', 66, 19, 11, 23),
                                                                                                                              (31, 'Wishes', 'Happy Easter', 67, 14, 5, 23),
                                                                                                                              (32, 'Clourful eggs', 'Look what I found instead of the bunny', 68, 27, 1, 23),
                                                                                                                              (33, 'Story of Light', 'Siluets of the night', 71, 0, 6, 24),
                                                                                                                              (34, 'Tungsten', 'Light bulbs', 72, 0, 5, 24),
                                                                                                                              (36, 'Mountain Landscape', 'Took this photo while hiking with my family', 74, 0, 11, 24),
                                                                                                                              (37, 'My Masterpiece', 'Vote for me!', 75, 0, 9, 24);
/*!40000 ALTER TABLE `photo_submissions` ENABLE KEYS */;

-- Dumping data for table photo_contest.ranks: ~4 rows (approximately)
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` (`rank_id`, `rank`) VALUES
                                            (1, 'Junkie'),
                                            (2, 'Enthusiast'),
                                            (3, 'Master'),
                                            (4, 'Wise and Benevolent Photo Dictator');
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;

-- Dumping data for table photo_contest.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role`) VALUES
                                            (1, 'Organizer'),
                                            (2, 'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table photo_contest.users: ~11 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `email`, `first_name`, `last_name`, `password`, `avatar_id`, `rank_id`, `role_id`, `points`, `reset_password_token`, `is_deleted`, `invitation_token`) VALUES
                                                                                                                                                                                                       (1, 'lilly', 'lilly@georgieff.bg', 'Lilly', 'Georgieva', '12345678', 38, 4, 2, 1102, '', 0, NULL),
                                                                                                                                                                                                       (2, 'lili123', 'lilstaykova@gmail.com', 'Lili', 'Staykova', '12345678', 1, 3, 2, 900, NULL, 0, '7AjzEyEmShP5Cn3Mo8XUiTACm9o5Vtgg7NBnCZMkK8mvK'),
                                                                                                                                                                                                       (3, 'LillyG', 'lili.staykova@yahoo.com', 'Tili', 'Staykova', '12345678', 1, 4, 2, 1200, NULL, 0, NULL),
                                                                                                                                                                                                       (4, 'mike_O', 'mike_O@email.bg', 'Mike', 'Parker', '12345678', 1, 3, 1, 900, NULL, 0, NULL),
                                                                                                                                                                                                       (5, 'cami84', 'cami84@email.com', 'Camilla', 'Chasey', '12345678', 1, 3, 2, 846, NULL, 0, NULL),
                                                                                                                                                                                                       (6, 'chaseFly', 'chaseFly@gmail.bg', 'Chase', 'Becker', '12345678', 1, 1, 2, 1, NULL, 0, NULL),
                                                                                                                                                                                                       (7, 'bianca_O', 'bianca_O@email.bg', 'Bianca', 'Nichols', '12345678', 33, 3, 1, 900, NULL, 0, NULL),
                                                                                                                                                                                                       (8, 'rocky_O', 'rocky_O@email.bg', 'Rocky', 'Mason', '12345678', 17, 3, 1, 900, NULL, 0, NULL),
                                                                                                                                                                                                       (9, 'john123', 'john@doe.bg', 'John', 'Doe', '12345678', 30, 3, 2, 577, NULL, 0, NULL),
                                                                                                                                                                                                       (10, 'Ivo', 'ivobankov758@gmail.com', 'Ivo', 'Bankov', '12345678', 1, 3, 2, 902, NULL, 0, ''),
                                                                                                                                                                                                       (11, 'jake22', 'atanasoff@gmail.bg', 'Jake', 'Atanasoff', '12345678', 54, 1, 2, 44, NULL, 0, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
