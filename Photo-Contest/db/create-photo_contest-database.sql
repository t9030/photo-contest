create table categories
(
    category_id int auto_increment
        primary key,
    category    varchar(50) null,
    constraint categories_category_id_uindex
        unique (category_id)
);

create table image_types
(
    type_id int auto_increment
        primary key,
    type    varchar(20) not null,
    constraint image_types_type_id_uindex
        unique (type_id),
    constraint image_types_type_uindex
        unique (type)
);

create table photos
(
    photo_id int auto_increment
        primary key,
    type_id  int      not null,
    content  longtext not null,
    constraint photos_photo_id_uindex
        unique (photo_id),
    constraint photos_image_types_type_id_fk
        foreign key (type_id) references image_types (type_id)
);

create table contests
(
    contest_id  int auto_increment
        primary key,
    title       varchar(50)          not null,
    category_id int                  null,
    is_open     tinyint(1) default 1 null,
    phase_1     datetime             null,
    phase_2     datetime             null,
    photo_id    int                  null,
    is_scored   tinyint(1) default 0 not null,
    constraint contests_contest_id_uindex
        unique (contest_id),
    constraint contests_title_uindex
        unique (title),
    constraint contests_categories_category_id_fk
        foreign key (category_id) references categories (category_id),
    constraint contests_photos_photo_id_fk
        foreign key (photo_id) references photos (photo_id)
);

create table ranks
(
    rank_id int auto_increment
        primary key,
    `rank`  varchar(100) not null,
    constraint ranks_rank_id_uindex
        unique (rank_id)
);

create table roles
(
    role_id int auto_increment
        primary key,
    role    varchar(100) null,
    constraint roles_role_id_uindex
        unique (role_id)
);

create table users
(
    user_id              int auto_increment
        primary key,
    username             varchar(30)          not null,
    email                varchar(100)         not null,
    first_name           varchar(30)          not null,
    last_name            varchar(30)          not null,
    password             varchar(50)          not null,
    avatar_id            int                  null,
    rank_id              int                  not null,
    role_id              int                  not null,
    points               int        default 0 null,
    reset_password_token varchar(45)          null,
    is_deleted           tinyint(1) default 0 not null,
    invitation_token     varchar(45)          null,
    constraint users_email_uindex
        unique (email),
    constraint users_user_id_uindex
        unique (user_id),
    constraint users_username_uindex
        unique (username),
    constraint users_photos_photo_id_fk
        foreign key (avatar_id) references photos (photo_id),
    constraint users_ranks_rank_id_fk
        foreign key (rank_id) references ranks (rank_id),
    constraint users_roles_role_id_fk
        foreign key (role_id) references roles (role_id)
);

create table contestants
(
    contest_id int null,
    user_id    int null,
    constraint contestants_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contestants_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table invited_users
(
    contest_id int null,
    user_id    int null,
    constraint invited_users_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint invited_users_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table juries
(
    contest_id int null,
    user_id    int null,
    constraint juries_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint juries_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table photo_submissions
(
    photo_submission_id int auto_increment
        primary key,
    title               varchar(100)  not null,
    story               longtext      not null,
    photo_id            int           not null,
    score               int default 3 null,
    author_id           int           null,
    contest_id          int           not null,
    constraint photo_submission_photo_submission_id_uindex
        unique (photo_submission_id),
    constraint photo_submissions_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint photo_submissions_photos_photo_id_fk
        foreign key (photo_id) references photos (photo_id),
    constraint photo_submissions_users_user_id_fk
        foreign key (author_id) references users (user_id)
);

create table comments
(
    comment_id    int auto_increment
        primary key,
    content       longtext      null,
    points        int default 0 null,
    submission_id int           null,
    jury_id       int           null,
    constraint comments_comment_id_uindex
        unique (comment_id),
    constraint comments_photo_submissions_photo_submission_id_fk
        foreign key (submission_id) references photo_submissions (photo_submission_id),
    constraint comments_users_user_id_fk
        foreign key (jury_id) references users (user_id)
);

create index users_avatars_avatar_id_fk
    on users (avatar_id);

create index users_ranking_ranking_id_fk
    on users (rank_id);

